﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class  testArrowScript : MonoBehaviour
{
    Rigidbody rb;
    public float shootForce;
    GameObject arrowss;
    public GameObject player;
    Ammo arrows;

    // Start is called before the first frame update
    void OnEnable()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
        ApplyForce();
        arrowss = GameObject.FindGameObjectWithTag("Arrow");
        arrows = player.GetComponent<Ammo>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        ApplyGravity();
    }

    void ApplyForce()
    {
        rb.AddRelativeForce(Vector3.forward * shootForce);
    }

    void ApplyGravity()
    {
        float yVelocity = rb.velocity.y;
        float xVelocity = rb.velocity.x;
        float zVelocity = rb.velocity.z;

        //Calculate the angle in which the object should fall down
        float combinedVelocity = Mathf.Sqrt(xVelocity * xVelocity + zVelocity * zVelocity);
        float fallAngle = -1 * Mathf.Atan2(yVelocity, combinedVelocity) * 180 / Mathf.PI;

        //Apply the angle
        transform.eulerAngles = new Vector3(fallAngle, transform.eulerAngles.y, transform.eulerAngles.z);
    }

    private void OnCollisionEnter(Collision other)
    {
        //Makes the object stick in the ground
        if (other.gameObject.CompareTag("Environment"))
        {
            rb.isKinematic = true;
            rb.useGravity = true;
        }
        if (other.gameObject.CompareTag("Player"))
        {
            if (rb.isKinematic == true)
            {
                //Player health should drop
                arrows.numArrows++;
            }
        }
        /*//On collision the arrow stops moving
        transform.GetComponent<ProjectileAddForce>().enabled = false;
        rb.velocity = Vector3.zero;
        rb.useGravity = false;
        rb.isKinematic = true;*/
    }
}