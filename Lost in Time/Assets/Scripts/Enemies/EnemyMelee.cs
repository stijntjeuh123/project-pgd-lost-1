﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
//using UnityEngine.Random;

public class EnemyMelee : EnemyBase
{

    private GameObject[] brutes; //Stores enemies
    private float distBetweenEnemies;
    private NavMeshAgent otherAgent;
    private bool callEnemiesFirstTime;
    private int random = 0;
    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        Debug.Log(anim + " ejfnjsekf");
        brutes = GameObject.FindGameObjectsWithTag("Enemies");
        //anim.SetBool("Melee", true);
        //anim.SetBool("isWalking", true);
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (healthBar.health < healthBar.maxHealth / 3)
        {
            CallEnemies();
        }

    }

    public override void FollowTarget() {

        base.FollowTarget();
        if (distance < agent.stoppingDistance)
        {
            if (anim != null)
            {
                //base.FaceTarget();
                anim.SetBool("Attack", true);
                anim.SetBool("isWalking", false);
                //base.isWandering = false;

          //      Debug.Log("anim" + anim + "efsefs");

              

                // move baack
                //agent.speed = 3;
                //anim.SetBool("isWalking", true);
                //agent.SetDestination(transform.position - new Vector3(0, 0, -5));
            }
        }

        else if (distance > agent.stoppingDistance)
        { // waneer de agent nog niet bij de player is,  loop animation en speed.
            if (anim != null)
            {
                //anim.SetBool("isWalking", true);
                //base.isWandering = true;
                anim.SetBool("isWalking", true);
                anim.SetBool("Attack", false);
            }
        }
    }

    private void CallEnemies()
    {

        //if wolf is in hearing range and one is howling, the others should come for help
        foreach (GameObject enemy in brutes)
        {
            if (gameObject != null)
            {
                distBetweenEnemies = Vector3.Distance(transform.position, enemy.transform.position);
                if (distBetweenEnemies <= hearRange)
                {
                    //enemies move to the destionation of the main enemy
                    otherAgent = enemy.GetComponent<NavMeshAgent>();

                    otherAgent.SetDestination(transform.position);
                }
                if (distBetweenEnemies < 3)
                {
                    //Enemies stop when near the main archer who called
                    otherAgent.isStopped = true;
                }
            }
        }
    }
}

