﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AxeEnemy : EnemyBase
{
    private GameObject[] brutes;
    private float distBetweenEnemies;
    private NavMeshAgent otherAgent;
    private Rigidbody rb;
    public bool isGuard;

    [SerializeField]
    private float attackRange;
    private int attack;
    private bool isAttacking;
    private int previousAttack;

    public override void Start()
    {
        base.Start();
        rb = GetComponent<Rigidbody>();
        brutes = GameObject.FindGameObjectsWithTag("Enemies");
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        Attacking();
        Walking();
        Backup();
        Dead();
        StartWandering();

        //Added by:Mitchell
        if (distance <= lookRange && (!playerHiding && !BehindWall()) && isGuard)
        {
            CallEnemies();
        }

        if ((distance > lookRange || playerHiding) && !lureGuard && isGuard)
        {
            agent.ResetPath();
            agent.SetDestination(startPos);
        }
        //anim.SetBool("SpinAttack", true);
    }

    //Check if is attacking
    private void Attacking()
    {
        //if player is close, enemy should attack
        if (distance <= attackRange) {
            anim.SetBool("Walking", false);
            if (isAttacking == false)
            {
                StartCoroutine("Attack");
            }
        }  
        else
        {
            anim.SetBool("Attack1", false);
            anim.SetBool("Attack2", false);
            anim.SetBool("SpinAttack", false);
            StopCoroutine("Attack");
            isAttacking = false;
        }
    }

    private IEnumerator Attack()
    {
        attack = Random.Range(1, 4);
        while (previousAttack == attack)
        {
            attack = Random.Range(1, 4);
        }
        previousAttack = attack;
        if (attack == 1)
        {
            anim.SetBool("SpinAttack", false);
            anim.SetBool("Attack2", false);
            anim.SetBool("Attack1", true);
        }
        else if (attack == 2)
        {
            anim.SetBool("SpinAttack", false);
            anim.SetBool("Attack1", false);
            anim.SetBool("Attack2", true);

        }
        else if (attack == 3)
        {
            anim.SetBool("Attack2", false);
            anim.SetBool("Attack1", false);
            anim.SetBool("SpinAttack", true);
        }
        isAttacking = true;
        yield return new WaitForSeconds(4f);
        isAttacking = false;

    }

    //Check if enemy is moving
    private void Walking()
    {
        if (rb.velocity != Vector3.zero && distance > attackRange)
        {
            anim.SetBool("Walking", true);
        }
    }

    //Check if enemy needs to backup
    private void Backup()
    {
        //When player attacks
        if(distance <= attackRange && Input.GetMouseButtonUp(0))
        {
            anim.SetBool("Backup", true);
        }
        else
        {
            anim.SetBool("Backup", false);
        }
    }

    //Check if enemy is dead
    private void Dead()
    {
        //if healthbar reaches 0
        if (healthBar.health <= 0)
        {
            anim.SetBool("isDeath", true);
            rb.velocity = Vector3.zero;
            isWandering = false;
            startWandering = false;
            agent.isStopped = true;
        }

    }

    //Check if the enemy got hit
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Weapons"))
        {
            anim.SetBool("GetHit", true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Weapons"))
        {
            anim.SetBool("GetHit", false);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, lookRange);

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(startPos, wanderRange);
    }

    private void CallEnemies()
    {
        //if wolf is in hearing range and one is howling, the others should come for help
        foreach (GameObject enemy in brutes)
        {
            if (gameObject != null)
            {
                distBetweenEnemies = Vector3.Distance(transform.position, enemy.transform.position);
                if (distBetweenEnemies <= hearRange)
                {
                    //enemies move to the destionation of the main enemy
                    otherAgent = enemy.GetComponent<NavMeshAgent>();

                    otherAgent.SetDestination(transform.position);
                }
                if (distBetweenEnemies < 3)
                {
                    //Enemies stop when near the main archer who called
                    otherAgent.isStopped = true;
                }
            }
        }
    }

    public override void StartWandering()
    {
        if (startWandering && !isGuard)
        {
            if (!isWandering)
            {
                StartCoroutine(Wander());
            }
            else if (isWandering)
            {
                //  transform.position
                transform.position += wanderDirection * Time.deltaTime * agent.speed / 2;
                Quaternion lookRotation = Quaternion.LookRotation(new Vector3(wanderDirection.x, 0, wanderDirection.z));
                transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 2f);
            }
        }
        else
        {
            StopCoroutine(Wander());
        }
    }
}