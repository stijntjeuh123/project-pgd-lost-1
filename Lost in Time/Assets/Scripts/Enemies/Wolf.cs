﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Wolf : EnemyBase
{
    private bool howl = false;
    private bool canHowl = true;

    private float distBetweenWolves;
    private float timerWolf;

    private GameObject[] wolves; //Stores all wolves
    private NavMeshAgent wolfAgent;

    public override void Start()
    {
        base.Start();
        wolves = GameObject.FindGameObjectsWithTag("Wolf");
    }

    public override void Update()
    {
        base.Update();
        StartHowling();
    }

    //Override to show animation
    public override void FollowTarget()
    {
        base.FollowTarget();
        if (distance < agent.stoppingDistance)
        {
            if (anim != null)
            {
                anim.SetBool("isRunning", false);
                anim.SetBool("isWalking", true);
                anim.SetBool("isAttacking", true);
                healthBarPlayer.DealDamage(0.1f);
            }
        }
        else
        {
            if (anim != null)
            {
                //anim.SetBool("isWalking", true);
                anim.SetBool("isRunning", false);
                anim.SetBool("isAttacking", false);
            }
        }
    }

    //Code for howling
    private void StartHowling()
    {
        //Howling
        if (healthBar.health < healthBar.maxHealth / 3)
        {
            StartCoroutine(Howl());
            startWandering = false;
            agent.speed = 0;
            timerWolf++;
            howl = true;

            //if wolf is in hearing range and one is howling, the others should come for help
            foreach (GameObject wolf in wolves)
            {
                if (gameObject != null)
                {
                    distBetweenWolves = Vector3.Distance(transform.position, wolf.transform.position);
                    if (distBetweenWolves <= hearRange && timerWolf < 120)
                    {
                        //Wolves should now move to the wolf that howled
                        wolfAgent = wolf.GetComponent<NavMeshAgent>();
                        wolfAgent.SetDestination(transform.position);
                        //Quaternion lookRotation = Quaternion.LookRotation(new Vector3(target.rotation.x, 0, target.rotation.z));
                        //wolf.transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 2f);
                    }
                    if (distBetweenWolves < 3)
                    {
                        //Wolves can walk away when they are close enough
                        wolfAgent.ResetPath();
                        //wolf.transform.rotation = gameObject.transform.rotation;
                    }
                }
            }
            if (timerWolf >= 120)
            {
                //Hurt wolf will walk as well
                agent.speed = 3;
                anim.SetBool("isWalking", true);
            }

        }
    }
    //Enemy should call for reinforcement
    IEnumerator Howl()
    {
        if (canHowl == true)
        {
            anim.SetBool("isHowling", true);
            audio.Play();
        }
        yield return new WaitForSeconds(1);
        canHowl = false;
        anim.SetBool("isHowling", false);
        howl = false;

    }

}
