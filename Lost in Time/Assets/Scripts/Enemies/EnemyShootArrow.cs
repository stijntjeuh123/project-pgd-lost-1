﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShootArrow : MonoBehaviour
{
    public GameObject arrowPrefab;
   // private GameObject arrow;
    public GameObject bow;
    public GameObject Arrow;
    public GameObject newArrow;
    public float pullSpeed = 50;
   // int arrows;
    float pullAmount = 0;
    private float maxTime = 3;
    private float minTime = 1;
    private float time;
    private float spawnTime;
   //public List<GameObject> Arrows;
    public bool shoot;
    bool arrowSlotted = false;
    int currentArrowInt;
   public bool coRoutineRunning;
    public GameObject player;
  private  Rigidbody arrowRB; //Arrows[(Arrows.Count - 1)].transform.GetComponent<Rigidbody>();
   private ProjectileAddForce arrowProjectile;
    private Vector3 targetDir;
    private float step;
    // The step size is equal to speed times frame time.
    //     float step = speed * Time.deltaTime;

   private Vector3 newDir;
         


    void Start()
    {
        //    SpawnArrow();
        //       SetRandomTime();
        //     time = minTime;

   /*     Arrows = new List<GameObject>();
        foreach (Transform child in transform)
        {
            if (child.gameObject.tag == "Arrow")
            {
                Debug.Log("AddArrow");
                Arrows.Add(child.gameObject);
            }
            */

   //     }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //ShootLogic();
    }

    /* void SpawnArrow()
     {
         if (numArrows > 0)
         {
             arrowSlotted = true;
             arrow = Instantiate(arrowPrefab, transform.position, transform.rotation) as GameObject;
             arrow.transform.parent = transform;
         }
     }*//*

     void SetRandomTime()
     {
         spawnTime = Random.Range(minTime, maxTime);
     }*/

    public IEnumerator DelayNextArrow()
    {
        yield return new WaitForSeconds(0.9f);
        coRoutineRunning = true;

        targetDir = player.transform.position - transform.position;

        // The step size is equal to speed times frame time.
        //     float step = speed * Time.deltaTime;
        step = Random.Range(50.0f, 100.0f);
      
        Debug.Log( "step!!!" + step);
          newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);
          Debug.DrawRay(transform.position, newDir, Color.red);
        Debug.Log(Quaternion.LookRotation(newDir));
        // Move our position a step closer to the target.
        //transform.rotation = Quaternion.LookRotation(newDir);

        newArrow = Instantiate(arrowPrefab, Arrow.transform.position, Quaternion.LookRotation(newDir));

        //   Arrows.Remove(Arrows[(Arrows.Count - 1)]);
        // Arrow = Arrows[Arrows.Count - 1];
        // currentArrowInt++;
        if (newArrow != null)
        {
            arrowRB = newArrow.transform.GetComponent<Rigidbody>(); //Arrows[(Arrows.Count - 1)].transform.GetComponent<Rigidbody>();
            arrowProjectile = newArrow.transform.GetComponent<ProjectileAddForce>();
            arrowProjectile.enabled = true;

            // newArrow.transform.parent = null;
            Debug.Log(" ArrowPROJ?" + arrowProjectile);
            Debug.Log(" ArrowPROJ?");
        }
        yield return new WaitForSeconds(2.7f);
        coRoutineRunning = false;
    }
    public void ShootArrow()
    {
        //Counts up
        // time += Time.deltaTime;

        //if (time >= spawnTime)
        //   {
        // newArrow =   Instantiate(arrowPrefab, Arrow.transform.position, Arrow.transform.rotation);
        StartCoroutine(DelayNextArrow());
        if (newArrow != null)
        {
            arrowRB = newArrow.transform.GetComponent<Rigidbody>(); //Arrows[(Arrows.Count - 1)].transform.GetComponent<Rigidbody>();
            arrowProjectile = newArrow.transform.GetComponent<ProjectileAddForce>();
            arrowProjectile.enabled = true;

            // newArrow.transform.parent = null;
            Debug.Log(" ArrowPROJ?" + arrowProjectile);
            Debug.Log(" ArrowPROJ?");
        }
        // Debug.Log(Arrows.Count);
        /*  if (Arrows.Count > 0)
              {

                  Arrow = Arrows[currentArrowInt];
                  arrowRB = Arrow.transform.GetComponent<Rigidbody>(); //Arrows[(Arrows.Count - 1)].transform.GetComponent<Rigidbody>();
                   arrowProjectile = Arrow.transform.GetComponent<ProjectileAddForce>();
                  arrowProjectile.enabled = true;
                  Arrow.transform.parent = null;
              Debug.Log("currARROWINT" + currentArrowInt + "TIJD" + Time.deltaTime * 10);


              Debug.Log("CurArrow" + currentArrowInt);
                  Debug.Log(" ArrowPROJ?" + arrowProjectile);
                  Debug.Log(" ArrowPROJ?");
                  //StartCoroutine(DelayNextArrow());
                  //Arrows.Remove(Arrows[(Arrows.Count - 1)]);
                /*  Arrows = new List<GameObject>();
                  foreach (Transform child in transform)
                  {
                      if (child.gameObject.tag == "Arrow")
                      {
                          Debug.Log("AddArrow");
                          Arrows.Add(child.gameObject);
                      }


                  }*/
        //  arrowRB.velocity = transform.forward * -10;
        //   if (pullAmount > 100)
      //  {

                 /*   pullAmount = 100;
                    Arrows[(Arrows.Count - 1)].SetActive(true); 
                   
                    Debug.Log(" ArrowPROJ?" + arrowProjectile);
                    Debug.Log(" ArrowPROJ?");
                    //   if (shoot)
                    {
                        pullAmount = Random.Range(40, 70);
                        // arrowSlotted = false;
                        arrowRB.isKinematic = false;
                        Arrows[(Arrows.Count - 1)].transform.parent = null;
                        //  numArrows--;
                        arrowProjectile.enabled = true;
                        arrowProjectile.shootForce = arrowProjectile.shootForce * ((pullAmount / 100) + 0.05f);


                        //      shoot = false;
                        //  time = 0;
                    }
                    /*  if (arrowSlotted == false)
                      {w
                          SpawnArrow();
                      }*/
                    //        }
                    //        }
                    //     }//else get arrows
                    // }}}}}
     //           }
       //     }
      //  }
    }
}