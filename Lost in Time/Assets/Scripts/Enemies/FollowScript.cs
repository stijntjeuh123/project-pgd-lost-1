﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FollowScript : MonoBehaviour
{
    public float lookRange;
    public int checkTime; //how long it takes for the guard to return to start

    public Transform target;
    NavMeshAgent agent;
    private Vector3 startPos;
    public bool playerHiding = false;
    public LayerMask wall;
    private bool lureGuard;
    private Vector3 lurePos;
    private WanderScript wanderScript;
    private int stamina = 50;
    private GameObject player;

    [HideInInspector]
    public bool startWandering;
    public EnemyShootArrow arrow;
    public Animator anim;
    private HealthBarPlayer healthBarPlayer;

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        agent = GetComponent<NavMeshAgent>();
        wanderScript = GetComponent<WanderScript>();
        player = GameObject.FindGameObjectWithTag("Player");
        healthBarPlayer = player.GetComponent<HealthBarPlayer>();
    }

    // Update is called once per frame
    void Update()
    {
		//if player is dead do not follow/chase and go back to wandering.
		/*if (healthBarPlayer.Value <= -0f){
			return;
		}*/

        float distance = Vector3.Distance(target.position, transform.position);

        //Move to player if he is in range
        if (distance <= lookRange && (!playerHiding && !BehindWall()))
        {
            agent.ResetPath();
            agent.SetDestination(target.position);

            
            if (distance < agent.stoppingDistance)
            {
                //Face the player 
                FaceTarget();
            }
            startWandering = false;
            arrow.shoot = true;
        }

        //Look in player's direction when enemy is in closer distance than stopping distance
        if (distance < agent.stoppingDistance)
        {
            //Face the player 
            FaceTarget();
            arrow.shoot = true;
            startWandering = false;
            if (anim != null)
            {
                anim.SetBool("isRunning", false);
                anim.SetBool("isWalking", true);
                anim.SetBool("isAttacking", true);
                healthBarPlayer.DealDamage(0.2f);
            }
        }
        else
        {
            if (anim != null)
            {
                //anim.SetBool("isWalking", true);
                anim.SetBool("isRunning", false);
                anim.SetBool("isAttacking", false);
            }
        }

        //go back to original position 
        if ((distance > lookRange || playerHiding) && !lureGuard && startWandering == false)
        {
            //StartCoroutine(ReturnToStart());
        }

        if (/*arrow.shoot == false &&*/ wanderScript.distance < wanderScript.wanderRange)
        {
            startWandering = true;
        }

        //Lure
        if (Input.GetKeyDown(KeyCode.L))
        {
            lureGuard = true;
            startWandering = false;
            agent.SetDestination(target.position);
            lurePos = target.position;
        }
        if (Vector3.Distance(lurePos, transform.position) < agent.stoppingDistance)
        {
            lureGuard = false;
        }
    }

    IEnumerator ReturnToStart()
    {
        yield return new WaitForSeconds(checkTime);
        agent.SetDestination(startPos);
    }

    public bool BehindWall()
    {
        if (target != null)
        {
            if (Physics.Linecast(transform.position, target.position, wall))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);

    }

    private void OnDrawGizmosSelected()
    {
        if (BehindWall())
        {
            Gizmos.color = Color.blue;
        }
        else
        {
            Gizmos.color = Color.red;
        }
        Gizmos.DrawWireSphere(transform.position, lookRange);
    }
}
