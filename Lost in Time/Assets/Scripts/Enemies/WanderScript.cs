﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WanderScript : MonoBehaviour
{
    public float wanderRange;
    public int minWanderTime;
    public int maxWanderTime;
    public HealthBar healthBar;
    public float hearRange;

    private Vector3 wanderCenter;
    public bool isWandering;
    private Vector3 wanderDirection;
    NavMeshAgent agent;
    NavMeshAgent wolfAgent;
    private FollowScript followScript;
    public bool isWolf;
    private bool canHowl = true;
    GameObject[] wolves; //Stores all wolves

    [HideInInspector]
    public float distance;
    public Animator anim;
    private float distBetweenWolves;
    private float timer;
    public bool howl = false;

    // Start is called before the first frame update
    void Start()
    {
        wanderCenter = transform.position;
        agent = GetComponent<NavMeshAgent>();
        followScript = GetComponent<FollowScript>();
        healthBar = GetComponent<HealthBar>();
        wolves = GameObject.FindGameObjectsWithTag("Wolf");

    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(wanderCenter, transform.position);
        if (followScript.startWandering)
        {
            if (!isWandering)
            {
                StartCoroutine(Wander());
                if (anim != null)
                    anim.SetBool("isWalking", true);

            }

            else if (isWandering)
            {
                transform.position += wanderDirection * Time.deltaTime * agent.speed / 2;
                Quaternion lookRotation = Quaternion.LookRotation(new Vector3(wanderDirection.x, 0, wanderDirection.z));
                transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 2f);
            }
        }
        else
        {
            StopCoroutine(Wander());
        }
        //Howling
        if (isWolf == true)
        {
            if (healthBar.health < healthBar.maxHealth / 3)
            {
                StartCoroutine(Howl());
                followScript.startWandering = false;
                agent.speed = 0;
                timer++;
                howl = true;
                //anim.SetBool("isWalking", false);

                //if wolf is in hearing range and one is howling, the others should come for help
                foreach (GameObject wolf in wolves)
                {
                    if (gameObject != null)
                    {
                        distBetweenWolves = Vector3.Distance(transform.position, wolf.transform.position);
                        if (distBetweenWolves <= hearRange)
                        {
                            //Wolves should now move to the wolf that howled
                            wolfAgent = wolf.GetComponent<NavMeshAgent>();
                            wolfAgent.SetDestination(transform.position);
                        }
                        if (distBetweenWolves < 3)
                        {
                            //Wolves can walk away when they are close enough
                            wolfAgent.isStopped = true;
                        }
                    }
                }
                if (timer >= 180)
                {
                    //Hurt wolf will walk as well
                    agent.speed = 3;
                    anim.SetBool("isWalking", true);
                }

            }
        }

    }

    IEnumerator Wander()
    {
        isWandering = true;
        int wanderTime = Random.Range(minWanderTime, maxWanderTime);

        if (distance >= wanderRange)
        {
            wanderDirection = -wanderDirection;
        }
        else
        {
            wanderDirection = new Vector3(Random.Range(-180, 180), 0, Random.Range(-180, 180)) / 180;
        }

        yield return new WaitForSeconds(minWanderTime);
        isWandering = false;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(wanderCenter, wanderRange);
    }

    //Enemy should call for reinforcement
    IEnumerator Howl()
    {
        if (canHowl == true)
        {
            anim.SetBool("isHowling", true);
        }

        yield return new WaitForSeconds(1);
        canHowl = false;
        anim.SetBool("isHowling", false);
        howl = false;

    }


}
