﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBase : MonoBehaviour
{
    //Various
    public Transform target;
    [SerializeField]
    private LayerMask wall;
    [HideInInspector]
    public NavMeshAgent agent;
    [HideInInspector]
    public GameObject player;
    [HideInInspector]
    public HealthBarPlayer healthBarPlayer;
    private WanderScript wanderScript;
    [HideInInspector]
    public Animator anim;
    [HideInInspector]
    public HealthBar healthBar;
    [HideInInspector]
    public AudioSource audio;
    private GameObject[] brutes; //Stores all wolves
    private NavMeshAgent bruteAgent;

    //floats
    public float lookRange;
    public float wanderRange;
    public float hearRange;
    [HideInInspector]
    public float distance; // distance between target and AI
    [HideInInspector]
    public float wanderDistance;
    private float distBetweenBrutes;
    private float timer;

    //ints
    public int minWanderTime;
    public int maxWanderTime;

    //bools
    [HideInInspector]
    public bool playerHiding = false;
    public bool lureGuard;
    [HideInInspector]
    public bool startWandering;
    [HideInInspector]
    public bool isWandering;
    private bool call = false;
    private bool canCall = true;

    //Vectors
    [HideInInspector]
    public Vector3 startPos;
    private Vector3 lurePos;
    [HideInInspector]
    public Vector3 wanderDirection;


    //Constructor
    /* public override void EnemyBase()
     {

     }*/

    //initialise variables
    public virtual void Start()
    {
        anim = GetComponent<Animator>();
        startPos = transform.position;
        agent = GetComponent<NavMeshAgent>(); //This script should be attached to the object with the agent
        player = GameObject.FindGameObjectWithTag("Player");
        healthBarPlayer = player.GetComponent<HealthBarPlayer>();
        healthBar = GetComponent<HealthBar>();
        brutes = GameObject.FindGameObjectsWithTag("Enemies");
        audio = GetComponent<AudioSource>();
    }

    public virtual void Update()
    {
        FollowTarget();
        StartWandering();
        //  Debug.Log("startWandering");
    }

    //All code to follow the target
    public virtual void FollowTarget()
    {
        distance = Vector3.Distance(target.position, transform.position);
        wanderDistance = Vector3.Distance(startPos, transform.position);

        //Move to player if he is in range when player is not hiding and not behind a wall
        if (distance <= lookRange && (!playerHiding && !BehindWall()))
        {
            agent.SetDestination(target.position);
            FaceTarget();
            //StartCalling();

            //If the player has already been followed and moves inside stopping distance, face him
            if (distance < agent.stoppingDistance)
            {
                //Face the player 
                FaceTarget();
            }
            startWandering = false;
        }
        else
        {
            agent.ResetPath();
            startWandering = true;
        }

        //Look in player's direction when enemy is in closer distance than stopping distance
        if (distance < agent.stoppingDistance)
        {
            //Face the player 
            FaceTarget();
            startWandering = false;
        }

        //go back to original position 
        if ((distance > lookRange || playerHiding) && !lureGuard)
        {
            //agent.ResetPath();
            //agent.SetDestination(startPos);
        }

        //If AI is within the wanderrange
        if (wanderDistance < wanderRange)
        {
            //startWandering = true;
        }

        //Lure all enemies in hearingrange
        if (Input.GetKeyDown(KeyCode.L))
        {
            if (distance < hearRange)
            {
                lureGuard = true;
                startWandering = false;
                agent.SetDestination(target.position);
                lurePos = target.position;
                Quaternion lookRotation = Quaternion.LookRotation(new Vector3(target.rotation.x, 0, target.rotation.z));
                transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 2f);
            }
            else
            {
                agent.ResetPath();
            }
        }

        if (Vector3.Distance(lurePos, transform.position) < agent.stoppingDistance)
        {
            lureGuard = false;
        }
    }

    //Check if player is behind a wall
    public virtual bool BehindWall()
    {
        if (target != null)
        {
            if (Physics.Linecast(transform.position, target.position, wall))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    //Face the target
    public virtual void FaceTarget()
    {
        //Get the direction to look at and then rotate the objects to that direction
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    //Makes AI wander
    public virtual void StartWandering()
    {
        if (startWandering)
        {
            if (!isWandering)
            {
                StartCoroutine(Wander());

            }
            else if (isWandering)
            {
                //  transform.position
                transform.position += wanderDirection * Time.deltaTime * agent.speed / 2;
                Quaternion lookRotation = Quaternion.LookRotation(new Vector3(wanderDirection.x, 0, wanderDirection.z));
                transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 2f);
            }
        }
        else
        {
            StopCoroutine(Wander());
        }
    }

    //Contains code what to do to wander
    public virtual IEnumerator Wander()
    {
        isWandering = true;
        if (anim != null)
        {
            anim.SetBool("isWalking", true);
        }
        int wanderTime = Random.Range(minWanderTime, maxWanderTime);

        if (wanderDistance >= wanderRange)
        {
            wanderDirection = new Vector3(Random.Range(-180, 180), 0, Random.Range(-180, 180)) / 180;
        }
        else
        {
            wanderDirection = new Vector3(Random.Range(-180, 180), 0, Random.Range(-180, 180)) / 180;
        }
        yield return new WaitForSeconds(minWanderTime);
        isWandering = false;
    }

    private void StartCalling()
    {
        //Howling
        //if (healthBar.health < healthBar.maxHealth / 3)
        //{
        StartCoroutine(Call());
        startWandering = false;
        Debug.Log(startWandering + "etst");
        agent.speed = 0;
        timer++;
        call = true;

        //if wolf is in hearing range and one is howling, the others should come for help
        foreach (GameObject Brute in brutes)
        {
            if (gameObject != null)
            {
                distBetweenBrutes = Vector3.Distance(transform.position, Brute.transform.position);
                if (distBetweenBrutes <= hearRange)
                {
                    //Wolves should now move to the wolf that howled
                    bruteAgent = Brute.GetComponent<NavMeshAgent>();
                    bruteAgent.SetDestination(transform.position);
                }
                if (distBetweenBrutes < 3)
                {
                    //Wolves can walk away when they are close enough
                    bruteAgent.isStopped = true;
                }
            }
        }
        if (timer >= 180)
        {
            //Hurt wolf will walk as well
            agent.speed = 3;
            anim.SetBool("isWalking", true);
        }

        //}
    }
    //Enemy should call for reinforcement
    IEnumerator Call()
    {
        if (canCall == true)
        {
            anim.SetBool("isCalling", true);
        }
        yield return new WaitForSeconds(1);
        canCall = false;
        anim.SetBool("isCalling", false);
        call = false;

    }

    //Draw gizmo's
    private void OnDrawGizmosSelected()
    {
        if (BehindWall())
        {
            Gizmos.color = Color.blue;
        }
        else
        {
            Gizmos.color = Color.red;
        }
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, lookRange);

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(startPos, wanderRange);

        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(transform.position, hearRange);
    }
}
