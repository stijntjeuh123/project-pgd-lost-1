﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyInventory : MonoBehaviour
{
    public GameObject[] inventory;

    // Start is called before the first frame update
    void Start()
    {
    }

    // changed to fixedupdate
    private void FixedUpdate()
    {
        if (Input.GetKeyDown("k"))
        {
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        for (int i = 0; i < inventory.Length; i++)
        {
            Instantiate(inventory[i], transform.position, inventory[i].transform.rotation);
        }
    }
}
