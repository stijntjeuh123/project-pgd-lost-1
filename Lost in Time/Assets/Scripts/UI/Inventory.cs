using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{

    #region Singleton

    public static Inventory instance;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of Inventory found!");
            return;
        }

        instance = this;
    }

    #endregion

    // Callback triggers when an item gets added/removed.
    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    public int space = 20;  // Amount of slots in inventory

    // Current list of items in inventory
    public List<Item> items = new List<Item>();
    public int currentItemInHand;
    public GameObject Hand;
    public void start()
    {

        Hand = GameObject.FindWithTag("Hand");


    }
    public void Update()
    {

        //  Debug.Log("loopie?  " + items.Count);
        for (int j = 0; j <= items.Count; j++)
        {
            //  Debug.Log("loop" + j + "items.count" + items.Count);
            if (Input.GetKeyDown("[" + j + "]") || Input.GetKeyDown("" + j))
            {
                Debug.Log("geberut er uberhaupt iets met de cijfers 1-10?" + j);
                currentItemInHand = j;
                items[j - 1].Wield();
                DisableOtherWeapons(j - 1);

                //  items[i - 1].active = true;
                items[j - 1].Use();
                //WieldOneItem(j -1);
                Debug.Log("using item nr" + j);
            }

            /*  if (Input.GetKeyDown("Keycode.Keypad" + j) || (Input.GetKeyDown("Keycode.Alpha" + j)))
          {
              Debug.Log("using item 2nd option" + j);
          }
          */
        }
    }

    public void DisableOtherWeapons(int currentItem)
    {

      //  Debug.Log("forLoopDisable" + Hand.transform.childCount);
        for (int i = 0; i < Hand.transform.childCount; i++)
        {

            Debug.Log("forLoopDisable" + i + "???" + Hand.transform.childCount);
            if (Hand.transform.GetChild(i).gameObject.name == items[currentItem].gameObject.name)
            {
                Debug.Log("doe aan? " + Hand.transform.GetChild(i).gameObject.name + "==" + items[currentItem].gameObject.name);
                Hand.transform.GetChild(i).gameObject.SetActive(true);
            }

            else if (Hand.transform.GetChild(i).gameObject.name != items[currentItem].gameObject.name)
            {
                Debug.Log("doe uit " + Hand.transform.GetChild(i).gameObject.name + "=" + items[currentItem].gameObject.name);
                Hand.transform.GetChild(i).gameObject.SetActive(false);
            }

            //   if (transform.GetChild(i).Hand.name == ) ;
            // transform.GetChild(i).gameObject.SetActive(false); // or false
        }
        /*for (int i = 0; i <= items.Count + 1; ++i)
        {
            Debug.Log("unwield 2/3" + i);
            if (i + 1 != currentItem)
            {
                Debug.Log("unwield 3/3/" + i);
                items[i - 1].Unwield();


                Debug.Log("unwielding item nr" + i);
            }
        } */
    }
    public void WieldOneItem(int currentItem)
    {
        Debug.Log("unwield 1/3");
        for (int i = 0; i <= items.Count + 1; ++i)
        {
            Debug.Log("unwield 2/3" + i);
            if (i + 1 != currentItem)
            {
                Debug.Log("unwield 3/3/" + i);
                items[i - 1].Unwield();


                Debug.Log("unwielding item nr" + i);
            }
            /*     else if (i == currentItemInHand)
                 {
                     items[i].Wield();
                 }
                     */
        }

    }

    // Add a new item. If there is enough room we
    // return true. Else we return false.
    public bool Add(Item item)
    {

      
        // Don't do anything if it's a default item
        if (!item.isDefaultItem)
        {
            // Check if out of space
            if (items.Count >= space)
            {
                Debug.Log("Not enough room.");
                return false;
            }

            items.Add(item);    // Add item to list
            DisableOtherWeapons(items.Count - 1);
            // Trigger callback
            if (onItemChangedCallback != null)
                onItemChangedCallback.Invoke();
        }

        return true;
    }

    // Remove an item
    public void Remove(Item item)

    {
      
        items[currentItemInHand].gameObject.transform.parent = null;
        Hand.transform.GetChild(currentItemInHand).gameObject.SetActive(false);
        //  Hand.transform.GetChild(currentItemInHand).DropItem();
     
        //    items[currentItemInHand].DropItem();
        
        items.Remove(item);     // Remove item from list
      
      //  Debug.Log("RemoveITem" + item);
        // Trigger callback
        if (onItemChangedCallback != null)
        {
            onItemChangedCallback.Invoke();
        }
    }

}
