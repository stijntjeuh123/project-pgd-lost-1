﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
public class HealthBar : MonoBehaviour
{
    public float maxHealth;
    // [HideInInspector]
    public float health;
    //ublic event Action<float> OnHealthPctChanged = delegate { };
    public GameObject mainObject;
    private bool dmgDone = false;

	public bool isDead = false;
    Animator m_Animator;
    bool Animate;
    public Image foregroundImage;
    private float updateSpeedSeconds = 0.5f;
    //public GameObject DamageColliderWC;
    //SphereCollider SPHERECOL_DamageColliderWC;

    // Start is called before the first frame update
    private void Start()
    {
        health = maxHealth;
        foregroundImage = GetComponentInChildren<Image>();  // 

        if (this.gameObject.GetComponent<Animator>() != null)
        {
            m_Animator = this.gameObject.GetComponent<Animator>();
               Animate = true;
        }
		//GetComponent<Collider>().enabled = false;
		//SPHERECOL_DamageColliderWC = DamageColliderWC.GetComponent<SphereCollider>();
		//SPHERECOL_DamageColliderWC = false;
    }


    private void DealDamage(float damage)
    {
        health -= damage;
        float currentHealthPct = health / maxHealth;
        //  print(currentHealthPct + " -1??   ");
        if (Animate)
        {
            m_Animator.SetTrigger("GetHitTrigger");

        }
        HandleHealthChanged(currentHealthPct);
           // OnHealthPctChanged(currentHealthPct);
        if (currentHealthPct <= -0)
        {
            m_Animator.SetTrigger("Die");
            Die();
        }
    }

    private void Die()
    {
		isDead = true;
		Debug.Log(isDead + " brute is dead");
        m_Animator.SetBool("isDeath", true);
        
        Destroy(mainObject);
    }

    private void HandleHealthChanged(float pct)
    {
        StartCoroutine(ChangeToPct(pct));
        Debug.Log("healthChangePCT" + pct);
    }

    private IEnumerator ChangeToPct(float pct)
    {
        Debug.Log("changeToCT" + pct);
        float preChangePct = foregroundImage.fillAmount;
        float elapsed = 0f;

        while (elapsed < updateSpeedSeconds)
        {
            Debug.Log("elapsed" + elapsed);
            elapsed += Time.deltaTime;
            foregroundImage.fillAmount = Mathf.Lerp(preChangePct, pct, elapsed / updateSpeedSeconds);

            yield return null;
        }

        //  foregroundImage.fillAmount = pct * 100;

    }

    void OnTriggerEnter(Collider collide)
    {
		//GetComponent<Collider>().enabled = true;
		if (collide.CompareTag("Melee")){
			
			DealDamage(0.5f);
			Debug.Log("testets");
			//SPHERECOL_DamageColliderWC.enabled = false;
			//Debug.Log(SPHERECOL_DamageColliderWC);
			//GetComponent<Collider>().enabled = true;
		}
        if (collide.CompareTag("Axe"))
        {
            DealDamage(0.4f);
            Debug.Log("collideAxe");
        }

        if (collide.transform.gameObject.name == ("Spear"))
        {
            DealDamage(1f);
            Debug.Log("collideSpear");
        }

        if (collide.transform.gameObject.name == ("Rock"))
        {
            DealDamage(0.7f);
            Debug.Log("collideRock");
        }


        if (collide.gameObject.layer == LayerMask.NameToLayer("PlayerArrow"))
        {
            DealDamage(1f);
            Debug.Log("collideArrow");
        }

        /*if (dmgDone == false)
        {
            if (collide.transform.tag == "PickUpObject")
            {
             //   DealDamage(5);
                dmgDone = true;
                Debug.Log("DAMG DONE");
            }
        }*/
    }

    void OnTriggerExit(Collider collide)
    {
		//GetComponent<Collider>().enabled = false;
        if (collide.gameObject.CompareTag("Melee"))
        {
			//GetComponent<Collider>().enabled = false;
        }
        if (collide.transform.tag == "PickUpObject")
        {
            dmgDone = false;
        }
    }

}
