﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectingPoint : MonoBehaviour
{
    private int totalObjects = 3;
    public int objectsFound =0;
    private GameObject gM;
    public Text scoreText;
    private Inventory inv;
    private ItemPickup itemP;

    private void Start()
    {
        gM = GameObject.FindGameObjectWithTag("Game_Manager");
        inv = gM.GetComponent<Inventory>();
        itemP = GameObject.FindGameObjectWithTag("PickUpObject").GetComponent<ItemPickup>();
    }

    private void LateUpdate()
    {
        scoreText.text = "Objects found:" + objectsFound + "/" + totalObjects;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PickUpObject"))
        {
            objectsFound++;
            inv.Remove(itemP.item);
            Destroy(other.gameObject);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("PickUpObject"))
        {
            //Decrease Score
            //objectsFound--;
        }
    }
}
