﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HealthBarPlayer : MonoBehaviour
{
    [SerializeField]
    public  float maxHealth;
    [SerializeField]
    public float health;
    public event Action<float> OnHealthPctChanged = delegate { };
    public GameObject mainObject;
    public Transform spawnPosition;

	public bool isDead = false;
    // Start is called before the first frame update
   private void Start()
    {
        maxHealth = 30;
        health = maxHealth;
    }

    private void Update()
    {
        float currentHealthPct = health / maxHealth;
        OnHealthPctChanged(currentHealthPct);
        if (currentHealthPct <= -0)
        {
            isDead = true;
            Die();
            Debug.Log(isDead);
            //SceneManager.LoadScene("main");
        }
    }

    public void DealDamage(float damage )
    {
        health -= damage;
        float currentHealthPct = health / maxHealth;
        OnHealthPctChanged(currentHealthPct);
     if (currentHealthPct <= -0)
        {
			isDead = true;
            Die();
			Debug.Log(isDead);
           //SceneManager.LoadScene("main");
        } 
    }


    public void heal(int damage)
    { 
        health -= damage;
        float currentHealthPct = health / maxHealth;
        OnHealthPctChanged(currentHealthPct);
        if (currentHealthPct <= -0)
        {
            SceneManager.LoadScene("Scene 1");
        }
    }

    private void Die()
    {
        Application.Quit();
        Debug.Log(isDead);
        transform.position = new Vector3(spawnPosition.position.x, spawnPosition.position.y, spawnPosition.position.z);
        // lose items? Destroy(mainObject);

    }

    void OnTriggerEnter(Collider collide)
    {

		if (collide.gameObject.CompareTag("MeleePlayer"))
		{
			DealDamage(5);

		}

        if (collide.gameObject.CompareTag("Arrow"))
        {
            DealDamage(3);

        }

        if (collide.transform.tag == "Spear")
        {
            DealDamage(5);
        }
    }

    void OnTriggerExit(Collider collide)
    {
		if (collide.gameObject.CompareTag("MeleePlayer"))
		{

		}

        if (collide.gameObject.CompareTag("Arrow"))
        {

        }

        if (collide.transform.tag == "Spear")
        {

        }
    }

}
