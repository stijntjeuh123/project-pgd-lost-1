﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarDraw: MonoBehaviour
{

    public Image foregroundImage;
    private float updateSpeedSeconds = 0.5f;
    private GameObject hBar;


    // Start is called before the first frame update
    void Awake()
    {
    //    hBar = GameObject.FindGameObjectWithTag("Player").GetComponent<Collider>().transform.root.GetComponent<HealthBar>().OnHealthPctChanged += HandleHealthChanged;
        
        foregroundImage = GetComponent<Image>();  // 
    }

    private void HandleHealthChanged(float pct)
    {
        StartCoroutine(ChangeToPct(pct));
        Debug.Log("healthChangePCT" + pct);
    }

    private IEnumerator ChangeToPct(float pct)
    {
        Debug.Log("changeToCT" + pct);
        float preChangePct = foregroundImage.fillAmount;
        float elapsed = 0f;

        while (elapsed < updateSpeedSeconds)
        {

            elapsed += Time.deltaTime;
            foregroundImage.fillAmount = Mathf.Lerp(preChangePct, pct, elapsed / updateSpeedSeconds);
 
            yield return null;
        }

      //  foregroundImage.fillAmount = pct * 100;

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
