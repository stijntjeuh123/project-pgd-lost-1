﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using AppFunc = System.Func<System.Collections.Generic.IDictionary<string, object>, System.Threading.Tasks.Task>;

public class ProjectileAddForce : MonoBehaviour
{
    Rigidbody rb;
    public float shootForce;
    GameObject arrowss;
    public GameObject player;
    Ammo arrows;
    protected float Animation;
    private Collider arrowCollider;
    // Start is called before the first frame update
    public Vector3 v3Current;
    private Vector3 v3To;
    private PickUpArrow pickUpArrow;

    void OnEnable()
    {
        v3Current = new Vector3(0, 0, 0); 
        arrowCollider = GetComponent<Collider>();
        pickUpArrow = GetComponent<PickUpArrow>();

        rb =  GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
    //    v3To = new Vector3(Random.Range(-3.0f, 3.0f), Random.Range(-3.0f, 3.0f), Random.Range(-3.0f, 3.0f));
        //       yRotation += Input.GetAxis("Horizontal");
        rb.constraints = RigidbodyConstraints.None;
        //transform.localEulerAngles = v3Current;
      //  transform.localEulerAngles = v3To;
        transform.Rotate(Random.Range(10f, 20.0f), Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
      //  transform.eulerAngles = new Vector3(10,  , 0);
        // this.transform.Rotate(this.transform.Rotation.x += Random.Range(-3.0f, 3.0f), this.transform.Rotation.y += Random.Range(-3.0f, 3.0f),   this.transform.Rotation.z += Random.Range(-3.0f, 3.0f), Space.Self);
        /*   this.transform.Rotation.x += Random.Range(-3.0f, 3.0f);
           this.transform.rotation.y += Random.Range(-3.0f, 3.0f);
           this.transform.rotation.z += Random.Range(-3.0f, 3.0f);*/
        ApplyForce();
        //arrowss = GameObject.FindGameObjectWithTag("Arrow");
       // arrows = player.GetComponent<Ammo>();
       
    
        arrowCollider.enabled = true;


        //  = new Vector3(this.transform.rotation.x + Random.Range(-3.0f, 1.0f), this.transform.rotation.y + Random.Range(-3.0f, 3.0f), this.transform.rotation.z + Random.Range(-3.0f, 3.0f));
        rb.constraints = RigidbodyConstraints.None;
    }

    // Update is called once per frame
    void Update()
    {
        ApplyGravity();
    }

    void ApplyForce()
    {
        //   Animation += Time.deltaTime;
        // Animation = Animation % 5f;
        //   transform.position = MathParabola.Parabola(transform.position, Vector3.forward * 10f, 3f, Animation / 5f);
        rb.constraints = RigidbodyConstraints.None;
        rb.AddRelativeForce(Vector3.forward * 10);
    //   rb.AddRelativeForce(Vector3.up * 1);
        arrowCollider.enabled = true;
       // Debug.Log(gameObject.name + "c0l" + arrowCollider + "position XYZ:" + transform.position.ToString("F4"));
      /*  this.transform.forward =
    Vector3.Slerp(this.transform.forward, rb.velocity.normalized, Time.deltaTime);*/
    }


    void ApplyGravity()
    {
        ApplyForce();
        //  Vector3 v = rigidbody3D.velocity;
        //    float angle = Mathf.Atan3(v.y, v.x) * Mathf.Rad3Deg;
        //  transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        /* float yVelocity = rb.velocity.y;
         float xVelocity = rb.velocity.x;
         float zVelocity = rb.velocity.z;

         //Calculate the angle in which the object should fall down
         float combinedVelocity = Mathf.Sqrt(xVelocity * xVelocity + zVelocity * zVelocity);
         float fallAngle = -1 * Mathf.Atan2(yVelocity, combinedVelocity) * 180 / Mathf.PI;

         //Apply the angle
         transform.eulerAngles = new Vector3(fallAngle, transform.eulerAngles.y, transform.eulerAngles.z); */
    }

    private void OnTriggerEnter(Collider other)
    {
      //  Debug.Log("CollidingWithSomething");
        //Makes the object stick in the ground
        if (other.gameObject.CompareTag("Environment"))
        {
          //  rb.isKinematic = true;
            rb.isKinematic = true;
           this.gameObject.layer = LayerMask.NameToLayer("Arrow");
          //   Debug.Log("Envionment+arrowCOLL");
            pickUpArrow.enabled = true;
          
        }
        if (other.gameObject.CompareTag("Player"))
        {
           // Debug.Log("Player+arrowCOLL");
          /*  if (rb.isKinematic == true)
            {
                //Player health should drop
                arrows.numArrows++;
            }*/
        }
        /*//On collision the arrow stops moving
        transform.GetComponent<ProjectileAddForce>().enabled = false;
        rb.velocity = Vector3.zero;
        rb.useGravity = false;
        rb.isKinematic = true;*/
    }

  /*  public static Vector3 Parabola(Vector3 start, Vector3 end, float height, float t)
    {
        Func<float, float> f = x => -4 * height * x * x + 4 * height * x;

        var mid = Vector3.Lerp(start, end, t);

        return new Vector3(mid.x, f(t) + Mathf.Lerp(start.y, end.y, t), mid.z);
    }

    */

}