﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
      if (Input.GetButtonDown("Use"))
        {
            RaycastHit hit;

            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 15))
            {
                //check what's hit
                ItemPickup itemPickup = hit.collider.GetComponent<ItemPickup>();
                Food apple = hit.collider.GetComponent<Food>();
                if (itemPickup != null && hit.collider.GetComponentInParent<Hand>() == null)
                {
                    //Debug.Log("parent is hand ???" +  hit.collider.GetComponentInParent<Hand>());
                    itemPickup.PickUp();
                    apple.Use();
                }
            }
        }
    }
}
