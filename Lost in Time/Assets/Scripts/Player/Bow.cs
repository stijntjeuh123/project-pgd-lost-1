﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityEngine.UI;

public class Bow : ItemPickup
{
    private ShootArrow shootArrow;
    bool inRange = false;
    public int damage;
    private float mouse;
    public float throwForce = 1000;
    private float dist;
    private GameObject crossHair;
    private AimCam aimCam;
    private Slider powerBar;
    private float powerBarThreshold = 5f;
    private float powerBarValue = 0f;
    private Renderer objectRenderer;
    public Item item;
    // Start is called before the first frame update
    /*  public override void Interact()
      {
          base.Interact();
          PickUp();
      }
      /*public virtual void PickUp()
      {
          Debug.Log("picking up an item" + item.name);
          Inventory.instance.Add(item);
          Destroy(gameObject);
      }*/



    private void Start()
    {
        shootArrow = GetComponent<ShootArrow>();
        rb = GetComponent<Rigidbody>();
        collider = GetComponent<Collider>();
        player = GameObject.FindGameObjectWithTag("Player");
        inventory = GameObject.FindGameObjectWithTag("Game_Manager").GetComponent<Inventory>();
        aimCam = player.GetComponent<AimCam>();
        powerBar = GameObject.FindGameObjectWithTag("PowerBar").GetComponent<Slider>();
        powerBar.minValue = 1f;
        powerBar.maxValue = 10f;
        powerBar.value = powerBarValue;
        getRenderer();

    }

    private void getRenderer()
    {
        objectRenderer = GetComponent<MeshRenderer>();
        // Debug.Log("what's wrong?" + objectRenderer);
        if (objectRenderer == null)
        {
            objectRenderer = GetComponentInChildren<MeshRenderer>();
            //Debug.Log("should unwield (NOTNULL) +" + this.gameObject + "renderer" + GetComponentInChildren<Renderer>());
        }

    }


    public void Use()
    {
        // wield the item
        Wield();
    }

    public void Unwield()
    {
        Debug.Log("objectUnwieldCalled" + this.gameObject);
        if (objectRenderer == null)
        {
            getRenderer();
        }
        if (objectRenderer != null)
        {
            objectRenderer.enabled = false;
            Debug.Log("disabling?");
        }
    }

    void FixedUpdate()
    {
        // If the object is moving and has been released, rotate the object downwards
        if (rb.velocity != Vector3.zero && released == true)
        {
            rb.rotation = Quaternion.LookRotation(rb.velocity.normalized);
        }
        //aimBow();
    }

    public void aimBow()
    {
        if (beingCarried)
        {
            //When button is being held down and aim is true powerbar will rise
            if (Input.GetMouseButton(0) && aimCam.aim == true)
            {
                powerBarValue += powerBarThreshold * Time.deltaTime;
                powerBar.value = powerBarValue;
            }
            //When button is released, object will be thrown
            if (Input.GetMouseButtonUp(0) && aimCam.aim == true)
            {
                shootArrow.enabled = true; ///????? enable shoot script en schiet?
                    // shoot logic
                rb.isKinematic = false;
                //  transform.parent = null;
                //Makes the object shoot forwards //arrow reference here??!??!
                // rb.AddForce(gameObject.transform.forward * throwForce * powerBarValue);
                // beingCarried = false;
                //   released = true;
                //inventory.Remove(this.item);
                powerBarValue = 0;
                powerBar.value = powerBarValue;
                //   collider.enabled = true;
            }
            /*Drop Item
             * else if (Input.GetMouseButtonDown(1))
            {
                rb.isKinematic = false;
                transform.parent = null;
                beingCarried = false;
            }*/
        }
    }


    public override void PickUp()
    {
        //   if (item.tag == || item.tag)
        // Inventory.instance.Add(item);
        //Wield();
        Debug.Log("picking up an item" + item.name);
        Inventory.instance.Add(item);
        //Destroy(gameObject);
    }

    void OnCollisionEnter(Collision other)
    {
        //If object hits anything else than player it should stick
        if (other.collider != player.GetComponent<Collider>())
        {
            if (transform.parent == null)
            {
                transform.parent = other.gameObject.transform;
                rb.isKinematic = true;
            }
        }
    }


}
/* public virtual void Use()
 {

     // override and make use method (onclick or hotkey) 
     // USE this to equip weapons/shield.
 }
 */
// Update is called once per frame


