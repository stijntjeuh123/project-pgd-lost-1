﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementPlayer : MonoBehaviour {

    private float moveFB;
    private float moveLR;

    public float speed = 10f;
    public float rotateSpeedX = 5f;
    public float rotateSpeedY = 2f;

    private CharacterController player;
    //public GameObject cam;

    private float verticalVelocity;
    public float gravity = 14f;
    public float jumpForce = 10f;



    // Use this for initialization
    void Start () {
        player = GetComponent<CharacterController>();

	}
	
	// Update is called once per frame
	void Update () {
        MoveHandler();
        Crouch();
		Crawl();
    }

    void Crouch()
    {
        if (Input.GetKey(KeyCode.LeftControl))
        {
            speed *= 0.8f;
        }
    }

	void Crawl()
	{
		if (Input.GetKey(KeyCode.L))
		{
			speed *= 0.4f;
		}
	}

    void MoveHandler()
    {
        moveFB = Input.GetAxis("Vertical") * speed;
        moveLR = Input.GetAxis("Horizontal") * speed;

        if (player.isGrounded)
        {
            verticalVelocity = -gravity * Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                verticalVelocity = jumpForce;
            }
        }
        else
        {
            verticalVelocity -= gravity * Time.deltaTime;
        }

        Vector3 direction = new Vector3(moveLR, verticalVelocity, moveFB);
        //transform.Rotate(0, rotX, 0);
       // cam.transform.localRotation = Quaternion.Euler(rotY, 0, 0);

        direction = transform.rotation * direction;
        player.Move(direction * Time.deltaTime);
    }
}
