﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityEngine.UI;

public class PickUpObject : MonoBehaviour
{
    public Transform player;
    public Transform socket;
    bool inRange = false;
    bool beingCarried = false;
    public int damage;
    private float mouse;
    public float throwForce = 200;
    private float dist;
    Rigidbody rb;
    private GameObject crossHair;
    private bool released;
    Collider collider;
    private GameObject aimCam;
    private AimCam aimCamm;
    private Slider powerBar;
    private float powerBarThreshold = 5f;
    private float powerBarValue = 0f;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        collider = GetComponent<Collider>();
        aimCam = GameObject.FindGameObjectWithTag("Player");
        aimCamm = aimCam.GetComponent<AimCam>();
        powerBar = GameObject.FindGameObjectWithTag("PowerBar").GetComponent<Slider>();
        powerBar.minValue = 1f;
        powerBar.maxValue = 10f;
        powerBar.value = powerBarValue;
    }

    // Update is called once per frame
    void Update()
    {
        dist = Vector3.Distance(gameObject.transform.position, player.position);
        if (dist <= 5f)
        {
            inRange = true;
        }
        else
        {
            inRange = false;
        }
        //Picks up all objects in range
        if (inRange && Input.GetButtonDown("Use"))
        {
            Pickup();
        }
    }

    void FixedUpdate()
    {
        //If the object is moving and has been released, rotate the object downwards
        if (rb.velocity != Vector3.zero && released == true)
        {
            rb.rotation = Quaternion.LookRotation(rb.velocity.normalized);
        }
        ThrowObject();

    }

    public void ThrowObject()
    {
        if (beingCarried)
        {
            if (Input.GetMouseButtonUp(0) )
            {
                rb.isKinematic = false;
                transform.parent = null;
                //Makes the object shoot forwards
                rb.AddForce(gameObject.transform.forward * throwForce  );
                beingCarried = false;
                released = true;
                collider.enabled = true;
                powerBarValue = 0;
                powerBar.value = powerBarValue;
            }
        }
    }

    void Pickup()
    {
        RaycastHit hit;
        //Shoots a ray forward from cam's position and checks if it hits something
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 100))
        {
            if (hit.collider.tag == "PickUpObject")
            {
                //Checks if the player already has a child object (weapon)
                if (socket.transform.childCount == 0)
                {
                    //collider.enabled = false;
                    GetComponent<Rigidbody>().isKinematic = true;
                    transform.parent = socket;
                    transform.position = socket.position;
                    transform.rotation = socket.rotation;
                    beingCarried = true;
                    released = false;
                }
            }

        }
    }

    void OnCollisionEnter(Collision other)
    {
        //If object hits anything else than player it should stick
        if (other.collider != player.GetComponent<Collider>())
        {
            if (transform.parent == null)
            {
                transform.parent = other.gameObject.transform;
                rb.isKinematic = true;
            }
        }
    }
}
