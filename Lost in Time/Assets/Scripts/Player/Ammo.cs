﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ammo : MonoBehaviour
{
    public int numArrows;
    public bool gotArrow;
    public GameObject ammoText;
    private Text ammoTextChange;

    // Start is called before the first frame update
    public void Start()
    {
        ammoTextChange = ammoText.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
      ammoTextChange.text = "Ammo: " + numArrows;

        //Debug.Log(numArrows);
        //print(gotArrow);
        if (gotArrow)
        {
            numArrows++;
            gotArrow = false;
        }
    }
}
