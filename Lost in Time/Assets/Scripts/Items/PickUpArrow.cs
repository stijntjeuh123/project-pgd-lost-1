﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpArrow : ItemPickup
{
    public Ammo ammoScript;
    private Rigidbody rb;
    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        GameObject g = GameObject.FindGameObjectWithTag("Player");
        ammoScript = g.GetComponent<Ammo>();
        rb = GetComponent<Rigidbody>();
    }

    public override void PickUp()
    {
        ammoScript.gotArrow = true;
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Environment"))
        {
            transform.parent = other.gameObject.transform;
            rb.isKinematic = true;
            gameObject.layer = 0;
        }
    }
}