﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityEngine.UI;

public class ThrowableObject : ItemPickup
{
    private AimCam aimCam;
    private Slider powerBar;
    private Animator anim;
    private Image powerBarObj;

    [SerializeField]
    private float throwForce;
    private float powerBarThreshold = 5f;
    private float powerBarValue = 0f;

    public override void Start()
    {
        base.Start();
        anim = player.GetComponent<Animator>();
        aimCam = player.GetComponent<AimCam>();
        powerBar = GameObject.FindGameObjectWithTag("PowerBar").GetComponent<Slider>();
        powerBarObj = GameObject.FindGameObjectWithTag("Fill").GetComponent<Image>();
        collider = GetComponent<Collider>();
        powerBar.minValue = 1f;
        powerBar.maxValue = 10f;
        powerBar.value = powerBarValue;
        powerBarObj.enabled = false;
    }

    public virtual void FixedUpdate()
    {
        // If the object is moving and has been released, rotate the object downwards
        if (rb.velocity != Vector3.zero && released == true)
        {
            rb.rotation = Quaternion.LookRotation(rb.velocity.normalized);
        }
        ThrowObject();
    }

    public void ThrowObject()
    {
        if (beingCarried && isThrowable)
        {
            //When button is being held down and aim is true powerbar will rise
            if (Input.GetMouseButton(0) && aimCam.aim == true)
            {
                powerBarObj.enabled = true;
                anim.SetBool("Throwing", true);
                //Debug.Log(anim.GetBool("isThrowing"));
                powerBarValue += powerBarThreshold * Time.deltaTime;
                powerBar.value = powerBarValue;
            }
            //When button is released, object will be thrown
            if (Input.GetMouseButtonUp(0) && aimCam.aim == true)
            {
                anim.SetBool("IsThrowing", true);
                anim.SetBool("Throwing", false);
                released = true;
                rb.isKinematic = false;
                transform.parent = null;
                //Makes the object shoot forwards from the camera (to the crosshair)
                rb.AddForce(Camera.main.transform.forward * throwForce * powerBarValue);

                //Reset powerbar and boolean values
                inventory.Remove(this.item);
                powerBarValue = 0;
                powerBar.value = powerBarValue;
                collider.enabled = true;
                beingCarried = false;
                powerBarObj.enabled = false;
            }
            if (aimCam.aim == false)
            {
                anim.SetBool("IsThrowing", false);
                anim.SetBool("Throwing", false);
                powerBarObj.enabled = false;
            }
        }
    }
}