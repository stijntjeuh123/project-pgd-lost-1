﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShinyTimePieces : MonoBehaviour
{
    private int currentShrinkIteration;
    private int amountOfTimesToShrink;
   // public ProtectCameraFromWallClip camScript1;
   //private FreeLookCam camScript2;
    private GameObject Camera1;
    private GameObject IntroCamera;
    /// </summary>
  // private List<script> list
    // Start is called before the first frame update
    void Start()
    {
        IntroCamera = GameObject.Find("IntroCamera");
       
        Camera1 = GameObject.Find("Camera");
        Camera1.SetActive(false);
        amountOfTimesToShrink = 1;
//Camera1.GetComponents<MonoBehaviour>();

        //camScript1 = Camera1.GetComponent<ProtectCameraFromWallClip>();
        //camScript2 = Camera1.GetComponent<FreeLookCam>();
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    public void SetComponentEnabled(Component component, bool value)
    {
        if (component == null) return;
        if (component is Renderer)
        {
            (component as Renderer).enabled = value;
        }
        else if (component is Collider)
        {
            (component as Collider).enabled = value;
        }
        else if (component is Animation)
        {
            (component as Animation).enabled = value;
        }
        else if (component is Animator)
        {
            (component as Animator).enabled = value;
        }
        else if (component is AudioSource)
        {
            (component as AudioSource).enabled = value;
        }
        else if (component is MonoBehaviour)
        {
            (component as MonoBehaviour).enabled = value;
        }
        else
        {
            Debug.Log("Don't know how to enable " + component.GetType().Name);
        }

    }
        void OnCollisionEnter(Collision col)
    {


        if (currentShrinkIteration < amountOfTimesToShrink)
        {

            IntroCamera.SetActive(false);
            Camera1.SetActive(true);
            this.gameObject.transform.localScale = new Vector3(0.002f, 0.002f, 0.002f);

            foreach (Component component in Camera1.GetComponentsInChildren(typeof(Component)))
            {
                SetComponentEnabled(component, true);
            }
            /*  MonoBehaviour[] mbehvs = gameObject.GetComponents(typeof(MonoBehaviour));
              foreach (MonoBehaviour mb in mbehvs)
              {
                  mb.SetActive = true;
              }*/
            //  camScript1.enabled = true;
            // camScript2.enabled = true;
        }
      //  Debug.Log("On the ground");
    }
    void OnCollisionExit(Collision col)
    {
        Debug.Log("On the ground");
    }
}
