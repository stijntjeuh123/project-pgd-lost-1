﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityEngine.UI;

public class MeleeableObject : ItemPickup
{
	bool inRange = false;
	private Renderer objectRenderer;

    private Animator anim;
    


    public override void Start()
    {
        base.Start();
        anim = player.GetComponent<Animator>();
        collider = GetComponent<Collider>();
    }

    void FixedUpdate()
	{
		// If the object is moving and has been released, rotate the object downwards
		if (rb.velocity != Vector3.zero && released == true)
		{
			rb.rotation = Quaternion.LookRotation(rb.velocity.normalized);
		}
		MeleeObject();

	}

    public void MeleeObject()
    {
        //string targetAnim = null;
        if (beingCarried)
        {
            //When button is being held down and aim is true powerbar will rise
            if (Input.GetMouseButton(0))
            {
                Debug.Log("teststs");
                anim.SetBool("Attack", true);
            }

            if (Input.GetMouseButtonUp(0))
            {
                anim.SetBool("Attack", false);
            }


            if (Input.GetMouseButton(1))
            {
                anim.SetBool("Block", true);
            }

            if (Input.GetMouseButtonUp(1))
            {
                anim.SetBool("Block", false);
            }

        }
    }


}