﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityEngine.UI;

public class ItemPickup : MonoBehaviour
{
    public GameObject player;
    public Item item;
    [HideInInspector]
    public Transform socket;

    [HideInInspector]
    public bool beingCarried = false;
    [HideInInspector]
    public bool released;
    [SerializeField]
    private bool isPiece;
    [SerializeField]
    private bool isAxe;
    [SerializeField]
    private bool isBow;
    [SerializeField]
    public bool isThrowable;

    [HideInInspector]
    public Rigidbody rb;
    [HideInInspector]
    public Collider collider;
    [HideInInspector]
    public Inventory inventory;
    private GameObject bow;
    private GameObject bowString;
    private Vector3 offset;
    [HideInInspector]
    public AudioSource audio;
    private Animator animator;
    private GameObject arm;

    public virtual void Start()
    {
        rb = GetComponent<Rigidbody>();
        collider = GetComponent<Collider>();
        player = GameObject.FindGameObjectWithTag("Player");
        bow = GameObject.FindGameObjectWithTag("Bow");
        bowString = GameObject.FindGameObjectWithTag("String");
        inventory = GameObject.FindGameObjectWithTag("Game_Manager").GetComponent<Inventory>();
        offset = new Vector3(0, 5, 0);
        audio = player.GetComponent<AudioSource>();
        animator = player.GetComponent<Animator>();
        arm = GameObject.FindGameObjectWithTag("Arm");
    }

    public void GetSocket()
    {
        socket = GameObject.FindGameObjectWithTag("Hand").GetComponent<Transform>();
    }

    public virtual void Update()
    {
        //Drop();
        //Drop();
        if (socket)
        {
            AxeAnim();
        }
        if (isBow && beingCarried)
        {
            gameObject.transform.parent = socket.transform;
            for (int i = 0; i < socket.transform.childCount; i++)
            {
                if(socket.transform.GetChild(i).gameObject.name == "Bow")
                {
                    gameObject.transform.position = socket.position;
                    gameObject.transform.eulerAngles = socket.eulerAngles + offset;
                }
            }
        }
    }

    public void Wield()
    {
        if (!socket)
        {
            GetSocket();
        }
        GetComponent<Rigidbody>().isKinematic = true;
        if (!isBow && !isAxe)
        {
            gameObject.transform.parent = socket.transform;
            gameObject.transform.position = socket.position;
            gameObject.transform.rotation = socket.rotation;
        }

        beingCarried = true;
        released = false;
    }


    public virtual void PickUp()
    {
        Inventory.instance.Add(item);
        //Destroy(gameObject);
        Wield();
    }

    //Drop timepiece
    private void Drop()
    {
        if (isPiece && Input.GetMouseButtonUp(0))
        {
            transform.parent = null;
            rb.isKinematic = false;
            rb.AddForce(Camera.main.transform.forward * 100);
            inventory.Remove(this.item);
            
        }
    }

    //Animations for axe
    public void AxeAnim()
    {
        //Debug.Log(beingCarried);
        if (isAxe && beingCarried)
        {
            gameObject.transform.parent = socket.transform;
            gameObject.transform.position = arm.transform.position;
            gameObject.transform.rotation = arm.transform.rotation;
            collider.isTrigger = true;
            //Use attack 1 
            if (Input.GetMouseButtonUp(0))
            {
                animator.SetBool("AxeAtk1", true);
            }
            else
            {
                animator.SetBool("AxeAtk1", false);
            }
            //Use attack 2
            if (Input.GetMouseButtonUp(1))
            {
                animator.SetBool("AxeAtk2", true);
            }
            else
            {
                animator.SetBool("AxeAtk2", false);
            }
        }
        else
        {
            collider.isTrigger = false;
        }

    }

    void OnCollisionEnter(Collision other)
    {
        //If object hits anything else than player it should stick
        if (other.collider == GameObject.FindGameObjectWithTag("Environment") )
        {
            if (transform.parent == null)
            {
                transform.parent = other.gameObject.transform;
                rb.isKinematic = true;
                //audio.Play();
            }
        }
    }
}
