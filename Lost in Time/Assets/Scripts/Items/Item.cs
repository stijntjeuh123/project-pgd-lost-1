using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/* The base item class. All items should derive from this. */

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]
public class Item : ScriptableObject {

	new public string name = "New Item";	// Name of the item
	public Sprite icon = null;				// Item icon
    public GameObject gameObject;   //actual object
	public bool isDefaultItem = false;      // Is the item default wear?

    private Food food;
    private ThrowableObject throwableObject;
  //  public Interactable interactible;
        
   // public Script UseItemScript;
   public void Unwield()
    {
        //gameObject = this.gameObject;
        gameObject.SetActive(false);
      /*  throwableObject = gameObject.GetComponent<ThrowableObject>();

        //equip objects
        if (throwableObject)
        {
            throwableObject.Unwield();
            Debug.Log("unequipping! " + name);
        }*/
    }
    public void Wield()
    {
        gameObject.SetActive(true);
        throwableObject = gameObject.GetComponent<ThrowableObject>();
       
        //equip objects
     
    }
    // Called when the item is pressed in the inventory
    public void Use ()
	{
        gameObject.SetActive(true);
        // Use the item
        // Something might happen
        //  UseItemScript.Use();

        //Debug.Log(gameObject.GetComponent<Interactable>()); 
        //  {
        //  /    interactable = GetComponent<Interactable>();
        //  gameObject.GetComponent<Interactable>().Use();
        /*if (gameObject.GetComponent<ThrowableObject> != null) { gameObject.GetComponent<ThrowableObject>().Use();
            Debug.Log("eating! " + name);
        }*/
        //if (gameObject.GetComponent<Food>() != null)
        {

            //eat food objects
            food = gameObject.GetComponent<Food>();
                if (food) {
                food.Use();
              //  Debug.Log("eating! " + name);
            }

          throwableObject = gameObject.GetComponent<ThrowableObject>();

            //equip objects
            if (throwableObject)
            {
                throwableObject.Wield();
                Debug.Log("equgoo " + name);
            }
        }
    }

	public void RemoveFromInventory ()
	{
		Inventory.instance.Remove(this);
	}
	
}
