﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestScript : ItemPickup
{
    public GameObject[] loot;

    // Start is called before the first frame update
    public override void PickUp()
    {
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        for (int i = 0; i < loot.Length; i++)
        {
            GameObject instantiate = (GameObject) Instantiate(loot[i], new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z), loot[i].transform.rotation);
            instantiate.name = loot[i].name;
        }
    }
}
