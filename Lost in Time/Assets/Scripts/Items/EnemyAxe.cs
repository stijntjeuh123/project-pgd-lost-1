﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAxe : MonoBehaviour
{
    private GameObject player;
    private HealthBarPlayer healthBar;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        healthBar = player.GetComponent<HealthBarPlayer>();
    }

    //Check if the enemy got hit
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            healthBar.DealDamage(0.2f);
        }
    }
}
