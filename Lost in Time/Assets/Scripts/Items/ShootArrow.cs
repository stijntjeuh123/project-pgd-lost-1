﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityEngine.UI;

public class ShootArrow : MonoBehaviour
{
    public Rigidbody arrowPrefab;
    public GameObject player;
    public Transform spawn;
    private Animator anim;
    private Rigidbody arrow;
    private Animator bowAnim;
    private ItemPickup item;
    private ParticleSystem particle;

    private Ammo ammo;
    private AimCam aimCam;
    private Slider powerBar;
    private Image powerBarObj;

    [SerializeField]
    private float shootForce = 4;
    private float powerBarThreshold = 5f;
    private float powerBarValue = 0f;
    private bool released;
    private bool spawned;

    private void Start()
    {
        aimCam = player.GetComponent<AimCam>();
        powerBar = GameObject.FindGameObjectWithTag("PowerBar").GetComponent<Slider>();
        powerBarObj = GameObject.FindGameObjectWithTag("Fill").GetComponent<Image>();
        ammo = player.GetComponent<Ammo>();
        powerBar.minValue = 1f;
        powerBar.maxValue = 10f;
        powerBar.value = powerBarValue;
        anim = player.GetComponent<Animator>();
        bowAnim = GameObject.FindGameObjectWithTag("Bow").GetComponent<Animator>();
        item = GetComponent<ItemPickup>();
        powerBarObj.enabled = false;
        particle = arrowPrefab.GetComponentInChildren<ParticleSystem>();

    }


    private void FixedUpdate()
    {
        if (arrow != null)
        {
            // If the object is moving and has been released, rotate the object downwards
            if (arrow.velocity != Vector3.zero && released == true)
            {
                arrow.rotation = Quaternion.LookRotation(arrow.velocity.normalized);
            }
        }
    }

    private void Update()
    {
        Shoot();
    }

    private void Shoot()
    {
        //When button is being held down and aim is true powerbar will rise
        if (Input.GetMouseButton(0) && aimCam.aim == true)
        {
            powerBarObj.enabled = true;
            anim.SetBool("AimingBow", true);
            bowAnim.SetBool("AimBow", true);
            //Charge shot
            powerBarValue += powerBarThreshold * Time.deltaTime;
            powerBar.value = powerBarValue;
            released = false;
            arrowPrefab.isKinematic = true;
            if (spawned == false)
            {
                arrow = Instantiate(arrowPrefab, spawn.position, spawn.rotation) as Rigidbody;
                arrow.transform.parent = GameObject.FindGameObjectWithTag("String").transform;
                spawned = true;
            }

        }

        if (Input.GetMouseButtonUp(0) && aimCam.aim == true)
        {
            anim.SetBool("ShootBow", true);
            bowAnim.SetBool("ShootBow", true);
            //anim.SetBool("AimingBow", false);
            //Release arrow
            arrow.isKinematic = false;
            arrow.transform.parent = null;
            arrow.AddForce(Camera.main.transform.forward * shootForce * powerBarValue);

            ammo.numArrows--;
            powerBarValue = 0;
            powerBar.value = powerBarValue;
            released = true;
            spawned = false;
            //aimCam.aim = false;
            powerBarObj.enabled = false;
        }

        if(aimCam.aim == false)
        {
            anim.SetBool("AimingBow", false);
            anim.SetBool("ShootBow", false);

            bowAnim.SetBool("AimBow", false);
            bowAnim.SetBool("ShootBow", false);
            powerBarObj.enabled = false;
        }
    }

}