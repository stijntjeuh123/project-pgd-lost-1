﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    public class AimCam : MonoBehaviour
    {
        private GameObject player;
        [HideInInspector]
        public bool aim;
        public Transform rotator;
        public Transform rotator2;

        private void Start()
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }

        private void Update()
        {
            //Debug.Log(rotator.transform.childCount);
            //See if the player is aiming
            if (Input.GetMouseButton(1) && rotator.transform.childCount > 0 )
            {
                aim = true;
                //Debug.Log(aim);
            }
            else // If not, change back to non aiming character
            {
                aim = false;
            }

            rotator.transform.eulerAngles = player.transform.eulerAngles;
            if (aim == true)
            {
                //Get the camera angles
                Vector3 e = Camera.main.transform.eulerAngles;
                e.x = 0;
                Vector3 g = Camera.main.transform.eulerAngles;
                g.y = 0;

                //rotate object
                rotator.eulerAngles = new Vector3(g.x, rotator.eulerAngles.y, rotator.eulerAngles.z);
                
                //rotate player 
                player.transform.eulerAngles = e;
            }
        }
    }   
}
