﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ArcherEnemy : EnemyBase
{

    private float bowPull;
    private GameObject[] enemies; //Stores enemies
    private float distBetweenEnemies;
    private NavMeshAgent otherAgent;
    private bool callEnemiesFirstTime;
    private bool shooting;
  //  Vector2 velocity;
  //  Vector2 smoothDeltaPosition;
  //  float speed = 0f;
    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        enemies = GameObject.FindGameObjectsWithTag("Enemies");
 
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();

        if (agent != null)
        {
            Debug.Log("navVElocity??" + agent.speed);

            {
                if (agent.speed > 0)
                {
                    anim.SetBool("Moving", true);
                    anim.SetFloat("Velocity Z", agent.speed);
                }
                else
                {
                    anim.SetFloat("Velocity Z", 0);
                }
            }


        }
    }


    public override void FollowTarget()
    {
      base.FollowTarget();


    }


    //pulling of the string (affects animator)
    private void ShootBow()
    {
        if (shooting)
        {

            if (bowPull < 1)
            {
                bowPull++;
            }
            else
            {
                bowPull = 0;
            }

            if (bowPull < 100)
            {

            }
        
            anim.SetFloat("BowPull", bowPull);

            Debug.Log("BowPull: " + anim.GetFloat("BowPull"));
            Debug.Log("Velocity X: " + anim.GetFloat("Velocity X"));
            Debug.Log("Velocity Z: " + anim.GetFloat("Velocity Z"));
            StartCoroutine(ShootBowCo());
        }

    }

    //shoot, damage and wait 
    IEnumerator ShootBowCo()
    {
        anim.SetFloat("BowPull", bowPull);
        healthBarPlayer.DealDamage(0.2f);
        yield return new WaitForSeconds(Random.Range(0.5f, 5.0f)); // wait before shooting again

    }
     //Animate height with distance to player ?
    //animator.SetFloat("AimHorizontal", aimHorizontal); // 
    //animator.SetFloat("AimVertical", aimVertical);
    //wander with new animation
  //  public override void StartWandering()
   // {
        /*void OnAnimatorMove()
        {

            Vector3 newPosition = transform.position;
                newPosition.x += anim.GetFloat("AnimationSpeed") * Time.deltaTime;
          //  newPosition.x += animator.GetFloat("Runspeed") * Time.deltaTime;
            transform.position = newPosition;
                /*
            // Update position based on animation movement using navigation surface height
            Vector3 position = anim.rootPosition;
            position.y = agent.nextPosition.y;
            transform.position = position;' */
        // } 
        /*
        public override void FollowTarget()
        {
            base.FollowTarget();

            //Debug.Log("distance & agentstoppingDistance:" + distance + "agentStoppingDistance" + agent.stoppingDistance + "//" + distance / agent.stoppingDistance);
            //Debug.Log("distance & agentstoppingDistance:" + distance + "agentStoppingDistance" + agent.stoppingDistance + "//" + distance / agent.stoppingDistance);
            if (distance < 2)
            {
                if (anim != null)
                {
                    anim.SetBool("Moving", false);
                    anim.SetBool("Aiming", true);
                    ShootBow();
                    // move baack

                    Debug.Log(distBetweenEnemies + "dstance" + distance);
                    Debug.Log("Move BaCK");
                    //agent.speed = 3;
                    //anim.SetBool("isWalking", true);
                    //   agent.SetDestination(transform.position - new Vector3(0,0, -30));
                }
            }

            else if (distance > 1 + agent.stoppingDistance)
            { // waneer de agent nog niet bij de player is,  loop animation en speed.
                if (anim != null)
                {
                    //anim.SetBool("isWalking", true);

                    anim.SetBool("Moving", true);
                    anim.SetBool("Aiming", false);
                    anim.SetFloat("Velocity Z", (distance / agent.stoppingDistance));
                    //anim.SetFloat("AimHorizontal", bowRotationHorizontal);
                    //  anim.setFloat("AimVertical", bowRotationVertical);
                }
            }

            else if (distance < agent.stoppingDistance)
            {
                if (anim != null)
                {
                    callEnemiesFirstTime = true;
                    anim.SetBool("Moving", false);
                    anim.SetBool("Aiming", true);
                    ShootBow();
                    //  anim.SetBool("isWalking", true);
                    //  Debug.Log("distance & agentstoppingDistance:" +  distance + "agentStoppingDistance" + agent.stoppingDistance + "//" + distance/agent.stoppingDistance);
                    // anim.SetFloat("Velocity Z", (distance / agent.stoppingDistance)); 

                    // wtf doet dit hier...  healthBarPlayer.DealDamage(0.2f);
                }
            }
        }

        //call other archer when health is low or player is first spotted.
        private void CallArchers()
        {
            //Howling

            {
                // animation maybe in the future?

                //  startWandering = false;
                //  agent.speed = 0;



                //if wolf is in hearing range and one is howling, the others should come for help
                foreach (GameObject enemy in enemies)
                {
                    if (gameObject != null)
                    {
                        distBetweenEnemies = Vector3.Distance(transform.position, enemy.transform.position);
                        if (distBetweenEnemies <= hearRange)
                        {
                            //enemies move to the destionation of the main enemy
                            otherAgent = enemy.GetComponent<NavMeshAgent>();

                            otherAgent.SetDestination(transform.position);
                        }
                        if (distBetweenEnemies < 3)
                        {
                            //Enemies stop when near the main archer who called
                            otherAgent.isStopped = true;
                        }
                    }
                }


            }
        }

        //pulling of the string (affects animator)
        void ShootBow()
        {
            if (shooting)
            {

                if (bowPull < 1)
                {
                    bowPull++;
                }
                else
                {
                    bowPull = 0;
                }

                if (bowPull < 100)
                {

                }

                StartCoroutine(ShootBowCo());
            }
        }

        //shoot, damage and wait 
        IEnumerator ShootBowCo()
        {
            anim.SetFloat("BowPull", bowPull);
            healthBarPlayer.DealDamage(0.2f);
            yield return new WaitForSeconds(Random.Range(0.5f, 5.0f)); // wait before shooting again

        }

        //wander with new animation
        public override void StartWandering()
        {
            if (startWandering)
            {
                if (!isWandering)
                {
                    StartCoroutine(Wander());
                    //  if (anim != null)
                    Debug.Log("Archer is wandering!" + gameObject.name);
                    anim.SetBool("moving", true);
                }
                else if (isWandering)
                {
                    transform.position += wanderDirection * Time.deltaTime * agent.speed / 2;
                    Quaternion lookRotation = Quaternion.LookRotation(new Vector3(wanderDirection.x, 0, wanderDirection.z));
                    transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 2f);
                }
            }
            else
            {
                StopCoroutine(Wander());
            }
        }
        */
    }