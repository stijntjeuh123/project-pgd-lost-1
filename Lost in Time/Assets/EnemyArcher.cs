﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyArcher : EnemyBase
{

    private float bowPull;
    private GameObject[] enemies; //Stores enemies
    private float distBetweenEnemies;
    private NavMeshAgent otherAgent;
    private bool callEnemiesFirstTime;
    public GameObject Arrow;
    private Vector3 randomDirection;
    public int walkRadius = 100;
    private NavMeshHit hit;
    private Vector3 spawnPoint;
    private Vector3 finalPosition;
    private Quaternion lookRotation;
    private float currentSpeed;
    private Vector3 lastPosition;
   public EnemyShootArrow EnemyShootArrow;
    public GameObject arrowPrefab;
    // private GameObject arrow;
    public GameObject bow;
   // public GameObject Arrow;
    public GameObject newArrow;
    public float pullSpeed = 50;
    // int arrows;
    float pullAmount = 0;
    private float maxTime = 3;
    private float minTime = 1;
    private float time;
    private float spawnTime;
    //public List<GameObject> Arrows;
    public bool shoot;
    bool arrowSlotted = false;
    int currentArrowInt;
    public bool coRoutineRunning;
    public float runEnergy = 10;
    Rigidbody arrowRB; //Arrows[(Arrows.Count - 1)].transform.GetComponent<Rigidbody>();
    ProjectileAddForce arrowProjectile;
    private int timesEnemiesCalled = 0;
    int numArrows = 15;

    public Transform arrowSpawn;
    // Start is called before the first frame update
    public override void Start()                              
    {
        base.Start();
        numArrows = Random.Range(8, 16);
        agent.stoppingDistance = 10;
        EnemyShootArrow = this.GetComponentInChildren<EnemyShootArrow>();

        agent.updateRotation = false;
        enemies = GameObject.FindGameObjectsWithTag("Enemies");
        anim.SetBool("Archer", true);                              
        anim.SetBool("Moving", true);
        StartCoroutine("randomWanderDestination", walkRadius);
        StartWandering();
        startWandering = true;
        if (walkRadius < 3)
        {
            walkRadius = 100;

        }
        agent.speed = 1;


        spawnPoint = transform.position;
        //    checkTime = Random.Range(7.0f, 16.0f);
    }

    void FixedUpdate()
    {
        currentSpeed = Mathf.Lerp(currentSpeed, (transform.position - lastPosition).magnitude, 0.7f /*adjust this number in order to make interpolation quicker or slower*/);
        lastPosition = transform.position;

     
    }
    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (healthBar.health < healthBar.maxHealth / 3 || callEnemiesFirstTime && timesEnemiesCalled < 1)
        {
            timesEnemiesCalled++;
            CallArchers();
        }

        if (numArrows < 2) { 
            //hide behind wall or look for errors when rng rolls 1/
           
        
                agent.SetDestination(arrowSpawn.transform.position);
            anim.SetBool("Aiming", false);
            anim.SetBool("Moving", true);
            Quaternion lookRotation = Quaternion.LookRotation(agent.velocity.normalized);
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 2f);
            if (agent.remainingDistance < 14)
            {
                numArrows = 16;
            }
            // BehindWall();
        }
    }

    
    public override void FollowTarget()
    {
        //Debug.Log("curSp" + currentSpeed);
        //anim.SetFloat("Velocity Z", currentSpeed);
        


        //anim.SetFloat("Velocity Y", currentSpeed);
        //  anim.SetFloat("Velocity Z", (agent.velocity.magnitude *  3/ ((agent.speed / 2))));
        // anim.SetFloat("Velocity Y", (agent.velocity.magnitude *  3/ ((agent.speed / 2))));
        //Debug.Log("Vel" + agent.velocity.magnitude + " spd" + agent.speed);
        if (numArrows > 1)
        {
            distance = Vector3.Distance(target.position, transform.position);
            wanderDistance = Vector3.Distance(startPos, transform.position);
        //    anim.SetFloat("Velocity Z", (agent.velocity.magnitude / (agent.speed agent.speed * 6))); //bepaald de ren anim !

            if (agent.isStopped & !(distance < agent.stoppingDistance - 5))
            {
                agent.Resume();
            }
            //  distance for the player and random walking.


            else if (distance < agent.stoppingDistance - 8)
            { // waneer de agent te dicht bij de player is

                //Debug.Log("0ND??" + (agent.stoppingDistance - 7));
                if (anim != null)
                {
                    agent.Stop();
                    transform.position += Vector3.back * Time.deltaTime * 3f;
                    anim.SetFloat("Velocity Z", (agent.velocity.magnitude / agent.speed) * 4);
                    anim.SetBool("Aiming", false);
                    anim.SetBool("Moving", true);

                    // agent.speed = 1;
                    // anim.SetBool("isWalking", true);
                    /*  Vector3 toPlayer = player.transform.position - transform.position;
                      Vector3 targetPosition = -toPlayer;
                      agent.SetDestination(targetPosition);
                      Debug.Log(targetPosition); */

                    // agent.speed = 2;
                    //     StartCoRoutine(randomWanderDestination(walkRadius - 6));
                    //     StartCoroutine(randomWanderDestination(50, transform.forward * -10));

                    //   StartCoroutine("randomWanderDestination", 7,  );
                    //    anim.SetFloat("Velocity Z", (agent.velocity.magnitude / agent.speed) * 6);
                    //   agent.Resume();

                    // move back cause the player is too close and enemies are nearby.
                    //NavMeshAgent.Warp(transform.forward * -1 * Time.deltaTime);
                    //   Debug.Log("move back please");
                    startWandering = false;
                    isWandering = false;
                    FaceTarget();
                }
            }
            else if (distance < agent.stoppingDistance )
            {
                anim.SetFloat("Velocity Z", (agent.velocity.magnitude / agent.speed) * 3);
                //Debug.Log("1ND??" + distance + "agent.stoppingDistanc?" + agent.stoppingDistance);
                FaceTarget();
               // agent.speed = 1;
                // Debug.Log(transform.rotation);
                //   Debug.Log(transform.eulerAngles);
                anim.SetFloat("AimVertical", transform.eulerAngles.y / 100);
                if (anim != null && anim.GetBool("Aiming") == false)
                {
                    // Debug.Log(anim.GetBool("Aiming") == false);
                    if (!coRoutineRunning)
                    {
                        StartCoroutine(shootNextArrow());
                    }
                    // StartCoroutine(ShootBowCo());
                    //    ShootBow();
                    // move baack
                    //     agent.speed = 3;
                    //   anim.SetBool("Moving", false);

                    //   agent.SetDestination(transform.position - new Vector3(0, 0, -5));
                }
            }
            else if (distance <= lookRange && (!playerHiding && !BehindWall()))
            {
                callEnemiesFirstTime = true;
                anim.SetFloat("Velocity Z", (agent.velocity.magnitude / agent.speed) * 6);
                //Debug.Log("2ND??" + distance + "LOOKRANGE?" +  lookRange);


                agent.speed = 4;  /// velocity ( rennen anim changen)

                // anim.SetFloat("Velocity Z", (agent.velocity.magnitude / agent.speed) * 6);

                //   agent.ResetPath();
                anim.SetBool("Aiming", false);
                anim.SetBool("Moving", true);
                agent.SetDestination(player.transform.position);
                FaceTarget();
                StartCoroutine(decreaseRunEnergy());



                isWandering = false;
                startWandering = false;

            }/*

                /*  else  if (distance < agent.stoppingDistance - 3)
                   { // waneer de agent nog niet bij de player is,  loop animation en speed.
                       Debug.Log("3ND??");
                       if (anim != null)
                       {
                                  // agent.speed = 1;
                          // anim.SetBool("isWalking", true);
                         /*  Vector3 toPlayer = player.transform.position - transform.position;
                           Vector3 targetPosition = -toPlayer;
                           agent.SetDestination(targetPosition);
                           Debug.Log(targetPosition); */
             /*        anim.SetBool("Aiming", false);
                     anim.SetBool("Moving", true);
                     agent.speed = 2;
                     //     StartCoRoutine(randomWanderDestination(walkRadius - 6));
                     StartCoroutine(randomWanderDestination(50, transform.forward * -10));

                     //   StartCoroutine("randomWanderDestination", 7,  );
                     //    anim.SetFloat("Velocity Z", (agent.velocity.magnitude / agent.speed) * 6);
                     //   agent.Resume();

                     // move back cause the player is too close and enemies are nearby.
                     //NavMeshAgent.Warp(transform.forward * -1 * Time.deltaTime);
                     //   Debug.Log("move back please");
                     startWandering = false;
                     isWandering = false;
                     FaceTarget();*/
             //*/*/
             //     anim.SetBool("Aiming", false);
             //    anim.SetFloat("Velocity Z", (distance / agent.stoppingDistance));
             //anim.SetFloat("AimHorizontal", bowRotationHorizontal);
             //  anim.setFloat("AimVertical", bowRotationVertical);
             //}*/

            // Debug.Log("distance & agentstoppingDistance:" + distance + "agentStoppingDistance" + agent.stoppingDistance + "//" + distance / agent.stoppingDistance);





            //  else if (Vector3.Distance(startPos, transform.position);

            /*if (distance < agent.stoppingDistance)
            {
                if (anim != null)
                {

                    //reset normal walking HERE?


                    //    callEnemiesFirstTime = true;

                    //      StartCoroutine(ShootBowCo());
                    // ShootBow();
                    //  anim.SetBool("isWalking", true);
                    //  Debug.Log("distance & agentstoppingDistance:" +  distance + "agentStoppingDistance" + agent.stoppingDistance + "//" + distance/agent.stoppingDistance);
                    // anim.SetFloat("Velocity Z", (distance / agent.stoppingDistance)); 

                    // wtf doet dit hier...  healthBarPlayer.DealDamage(0.2f);
                }
            }*/
        }
    }

        public IEnumerator decreaseRunEnergy()
        {
            while (runEnergy > 1)
            {
             
                yield return new WaitForSeconds(10f);
            runEnergy -= 0.33f;

        }
            //increase after resting
            while (runEnergy < 1.5)
            {
                yield return new WaitForSeconds(20f);
                runEnergy += 3;

            }
        }

        public IEnumerator shootNextArrow()
        {
            //Debug.Log(" SHOOTINGARR");
       
            coRoutineRunning = true;

            anim.SetBool("Moving", false);
            anim.SetBool("Aiming", true);
            yield return new WaitForSeconds(1.50f);
            newArrow = null;
       
        newArrow = Instantiate(arrowPrefab, Arrow.transform.position, Arrow.transform.rotation);
        numArrows--;
        yield return new WaitForSeconds(0.05f); // before shooting

            //   Arrows.Remove(Arrows[(Arrows.Count - 1)]);
            // Arrow = Arrows[Arrows.Count - 1];
            // currentArrowInt++;



            arrowRB = newArrow.transform.GetComponent<Rigidbody>(); //Arrows[(Arrows.Count - 1)].transform.GetComponent<Rigidbody>();
            arrowProjectile = newArrow.transform.GetComponent<ProjectileAddForce>();
            arrowProjectile.enabled = true;
            anim.SetBool("Aiming", false);
            // newArrow.transform.parent = null;
            //  Debug.Log(" ArrowPROJ?" + arrowProjectile);
            //        Debug.Log(" ArrowPROJ?");

            yield return new WaitForSeconds(1.9f);
            anim.SetBool("Aiming", false);
            coRoutineRunning = false;
        }
    
        //call other archer when health is low or player is first spotted.
        private void CallArchers()
        {
            //Howling

            {
                // animation maybe in the future?

                //  startWandering = false;
                //  agent.speed = 0;



                //if wolf is in hearing range and one is howling, the others should come for help
                foreach (GameObject enemy in enemies)
                {
                    if (gameObject != null)
                    {
                        distBetweenEnemies = Vector3.Distance(transform.position, enemy.transform.position);
                        if (distBetweenEnemies <= hearRange)
                        {
                            //enemies move to the destionation of the main enemy
                            otherAgent = enemy.GetComponent<NavMeshAgent>();

                            otherAgent.SetDestination(transform.position);
                        }
                        if (distBetweenEnemies < 3)
                        {
                            //Enemies stop when near the main archer who called
                            otherAgent.isStopped = true;
                        }
                    }
                }

            
            }
        }

        //pulling of the string (affects animator)
        void ShootBow()
        {
            if (bowPull < 111)
            {
                bowPull++;

            }
            else
            {
                bowPull = 0;
            }

            //   StartCoroutine(ShootBowCo(walkRadius));
        }

        //shoot, damage and wait 
        IEnumerator ShootBowCo()
        {


            anim.SetBool("Moving", false);
            anim.SetBool("Aiming", true);
            //Debug.Log("AIMTRUE" + Time.deltaTime * 10);
            Arrow.SetActive(true);

            yield return new WaitForSeconds(0.5f);


            if (EnemyShootArrow != null)
            {

                // EnemyShootArrow.ShootArrow();
                //Debug.Log("SHOOTARR" + Time.deltaTime * 10);
                if (!EnemyShootArrow.coRoutineRunning)
                {
                    StartCoroutine(EnemyShootArrow.DelayNextArrow());
                }
            }
            else if (EnemyShootArrow == null)
            {
                EnemyShootArrow = this.GetComponentInChildren<EnemyShootArrow>();
                //     EnemyShootArrow.ShootArrow();
            }

            if (!EnemyShootArrow.coRoutineRunning)
            {
                EnemyShootArrow = this.GetComponentInChildren<EnemyShootArrow>();
                //   EnemyShootArrow.ShootArrow();
                // Debug.Log("SHOOTARR" + Time.deltaTime * 10);
                StartCoroutine(EnemyShootArrow.DelayNextArrow());
            }


            //   yield return new WaitForSeconds(3f);
            //    Debug.Log("TIME TO ARROW");
            // Arrow.parent = null;
            // Arrow.parent = null;
            // yield return new WaitForSeconds (3f);
            //   Arrow.transform.parent = null;
            //   Arrow.GetComponent<Rigidbody>().AddForce(Arrow.transform.forward * 1);


            //yield return new WaitForSeconds(Random.Range(1f, 3.0f)); // wait before turning anim off
            anim.SetBool("Aiming", false);
            // anim.SetFloat("BowPull", bowPull);
            //healthBarPlayer.DealDamage(0.2f);


        }
    IEnumerator randomWanderDestination(int walkRadius)
    {
        Vector3 oppositePlayerDirection = new Vector3(0, 0, 0);
        Vector3 emptyVec = new Vector3(0, 0, 0);
        if (oppositePlayerDirection != emptyVec)
        {
            randomDirection = oppositePlayerDirection;
            // Debug.Log(randomDirection);
        }
        else
        {
            randomDirection = spawnPoint + Random.insideUnitSphere * walkRadius;
            //Debug.Log("rd1");
        }

        if (NavMesh.SamplePosition(randomDirection, out hit, walkRadius, NavMesh.AllAreas))
        {
            finalPosition = hit.position;
            //Debug.Log(finalPosition);
            //Debug.Log("Pos berekend en egzet");
            agent.SetDestination(finalPosition);
            yield return new WaitForSeconds(Random.Range(4f, 5f));

        }
    }

        IEnumerator randomWanderDestinationOpposite(int walkRadius, Vector3 oppositePlayerDirection){
        Vector3 emptyVec = new Vector3(0, 0, 0);
        if (oppositePlayerDirection != emptyVec)
        {
            randomDirection = oppositePlayerDirection;
            // Debug.Log(randomDirection);
        }
        else
        {
            randomDirection = spawnPoint + Random.insideUnitSphere * walkRadius;
        //Debug.Log("rd1");
         }

        if (NavMesh.SamplePosition(randomDirection, out hit, walkRadius, NavMesh.AllAreas)){
            finalPosition = hit.position;
            //Debug.Log(finalPosition);
            //Debug.Log("Pos berekend en egzet");
            agent.SetDestination(finalPosition);
            yield return new WaitForSeconds(Random.Range(4f, 5f));

        }
               
      // agent.Warp(transform.position);
        
   //     otherAgent.
}
    //wander with new animation
    public override void StartWandering()
    {
        if (startWandering)
        {
            if (!isWandering)
            {
                StartCoroutine(randomWanderDestinationOpposite(walkRadius, new Vector3(0,0,0)));
                StartCoroutine(Wander());
                //  if (anim != null)
               // startCoRoutine(randomWanderDestination(4));
             //   randomWanderDestination(walkRadius);
//anim.SetBool("Moving", true);
            }
            else if (isWandering)
            {
           //     Debug.Log("curDSpeed" + currentSpeed);
                if (currentSpeed < 0.001)
                {
             //       anim.SetBool("Moving", false);
                }
              else  if (currentSpeed > 0.001)
                {
                    anim.SetBool("Moving", true);
                }
                //  if (!agent.isStopped)
                {
                 //   StartCoroutine(Wander());
                
                }

                if (agent.velocity != Vector3.zero)
                {
                    Quaternion lookRotation = Quaternion.LookRotation(agent.velocity.normalized);
                    transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 2f);
                }
              //  transform.rotation = Quaternion.LookRotation(agent.velocity.normalized);
                //     Debug.Log("iswandering1" );
                /*   Vector3 randomDirection = Random.insideUnitSphere * 10;
                   randomDirection += transform.position;
                   NavMeshHit hit;
                   NavMesh.SamplePosition(randomDirection, out hit, walkRadius, 1);
                   Vector3 finalPosition = hit.position;
                   */

                // agent.Warp += wanderDirection * Time.deltaTime * agent.speed / 2;
                //    StartCoroutine(Wander());
                //     lookRotation = Quaternion.LookRotation(new Vector3(randomDirection.x, 0, randomDirection.z));
                //    transform.rotation = lookRotation; //Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 2f);
            }
        }
        else
        {
            StopCoroutine(Wander());
        }
    }
    public override IEnumerator Wander()
    {
        StartCoroutine(randomWanderDestination(walkRadius));
     //   randomWanderDestination(walkRadius);
            isWandering = true;
       
        //  if (anim != null)
        //   {
        //    anim.SetBool("isWalking", true);
        //  }      Debug.Log("startWandering" + StartWandering);
        //Debug.Log("iswandering");
        int wanderTime = Random.Range(minWanderTime, maxWanderTime);
        agent.SetDestination(finalPosition);
        /*
        if (wanderDistance >= wanderRange)
        {
            wanderDirection = -wanderDirection;
        }
        else
        {
            wanderDirection = new Vector3(Random.Range(-180, 180), 0, Random.Range(-180, 180)) / 180;
        }*/
        yield return new WaitForSeconds(wanderTime);
        isWandering = false;
    }


}