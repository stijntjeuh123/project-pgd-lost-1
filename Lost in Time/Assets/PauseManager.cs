﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour
{
    private bool paused = false;
    private GameObject Player;
    private GameObject PauseMenu;
    // Start is called before the first frame update
    void Start()
    {
        PauseMenu = GameObject.Find("PauseMenu");
        PauseMenu.SetActive(false);
        //  PauseMenu = GameObject.Find("PauseMenu");
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Pause"))
        {
            if (!paused)
            {
                Time.timeScale = 1;
                PauseMenu.SetActive(false);
                paused = !paused;
                Debug.Log("PauseTRue");
            }

            else if (paused)
            {
                Time.timeScale = 0;
                PauseMenu.SetActive(true);
                paused = !paused;
                Debug.Log("Pausefalse");
                //  Debug.Log("PauseTRue");
            }
        }

        else if (Input.GetButtonDown("escape") && !paused)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        }

    }
}
    

