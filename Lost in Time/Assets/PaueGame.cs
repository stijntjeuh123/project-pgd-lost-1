﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour
{
    private bool paused = false;
    private GameObject Player;
    private GameObject PauseMenu;
    // Start is called before the first frame update
    void Start()
    {
        //  Player = GameObject.Find("Player");
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetButtonDown("Pause"))
        {
            if (paused)
            { Time.timeScale = 1; paused = false; }

            else
            {
                Time.timeScale = 0;
                paused = true;
                //  Debug.Log("PauseTRue");
            }
        }
    }
}

