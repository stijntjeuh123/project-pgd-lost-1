﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManager : MonoBehaviour
{
	[Header("Init")]
	public GameObject activeModel;

	[Header("Inputs")]
	//public float vertical;
	//public float horizontal;
	//public float moveAmount;
	//public Vector3 moveDirection;
	public bool rt, lt, rb, lb;

	[Header("Stats")]
	public float moveSpeed = 2;
	//public float runSpeed = 3.5f;

	[Header("States")]
	//public bool onGround;
	//public bool run;
	public bool inAction;
	//public bool canMove;
	public string damageWho;

	[HideInInspector]
	public Animator anim;
	[HideInInspector]
	public Rigidbody rigid;

	[HideInInspector]
	public float delta;
	public float _actionDelay;

    public float tapSpeed = 0.5f;
    public float lastTapTime = 0;

	public void Start(){
        //damageCollider = GetComponent<Collider>();
        lastTapTime = Time.time;
	}

    public void Update()
    {
        //DetectAction();
        string targetAnim = null;
        if (Input.GetKeyDown(KeyCode.D))
        {
            //Debug.Log((Time.time - lastTapTime) < tapSpeed);
            if ((Time.time - lastTapTime) < tapSpeed)
            {
                targetAnim = "2Hand-Axe-Dodge-Right";
                Debug.Log("Double tap");
                anim.CrossFade(targetAnim, 0.2f);
            }
            lastTapTime = Time.time;
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            //Debug.Log((Time.time - lastTapTime) < tapSpeed);
            if ((Time.time - lastTapTime) < tapSpeed)
            {
                targetAnim = "2Hand-Axe-Dodge-Left";
                Debug.Log("Double tap");
                anim.CrossFade(targetAnim, 0.2f);
            }
            lastTapTime = Time.time;
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            //Debug.Log((Time.time - lastTapTime) < tapSpeed);
            if ((Time.time - lastTapTime) < tapSpeed)
            {
                targetAnim = "2Hand-Axe-Roll-Backward";
                Debug.Log("Double tap");
                anim.CrossFade(targetAnim, 0.2f);
            }
            lastTapTime = Time.time;
        }

        if (Input.GetMouseButton(0))
        {
            targetAnim = "oh_attack_1";
        }

    }

	public void Init(){
		SetupAnimator();
		rigid = GetComponent<Rigidbody>();
	}

	void SetupAnimator(){
		if(activeModel == null){
			anim = GetComponentInChildren<Animator>();
			if(anim == null){
				Debug.Log("No model");
			} else {
				activeModel = anim.gameObject;
			}
		}
		if(anim == null){
			anim = activeModel.GetComponent<Animator>();
		}
	}

	public void FixedTick(float d){
		delta = d;

		DetectAction();
		if (inAction){
			_actionDelay += delta;
			if (_actionDelay > 0.3f){
				inAction = false;

				_actionDelay = 0;
			} else {
				return;	
			}	
		}
	}

	public void DetectAction(){
		if(rb == false && rt == false && lb == false && lt == false){
			return;
		}
			string targetAnim = null;

			if(rb){
				targetAnim = "oh_attack_1";
				anim.SetFloat("Forward", 0f);
			}
			if(rt){
				targetAnim = "oh_attack_2";
				anim.SetFloat("Forward", 0f);
			}
			if(lb){
				targetAnim = "oh_attack_3";
				anim.SetFloat("Forward", 0f);
			}
			if(lt){
				targetAnim = "oh_attack_1";
				anim.SetFloat("Forward", 0f);
			}

			if (string.IsNullOrEmpty(targetAnim)){
				return;
			}
			

        

        inAction = true;
        anim.CrossFade(targetAnim, 0.2f);
        rigid.velocity = Vector3.zero;
    }
}
