﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
	//float vertical;
	//float horizontal;
	//bool b_input;
	bool a_input;
	bool x_input;
	bool y_input;

	bool rb_input;
	float rt_axis;
	bool lb_input;
	float lt_axis;
	bool rt_input;
	bool lt_input;

	StateManager states;

	float delta;


    void Start()
    {
		states = GetComponent<StateManager>();
		states.Init();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
		delta = Time.fixedDeltaTime;
		GetInput();
		UpdateStates();
		states.FixedTick(Time.deltaTime);
    }

	void GetInput(){
		//vertical = Input.GetAxis("Vertical");
		//horizontal = Input.GetAxis("Horizontal");
		//b_input = Input.GetButton("b_input");
		rt_input = Input.GetButton("RT");
		rt_axis = Input.GetAxis("RT");
		if (rt_axis != 0){
			rt_input = true;

			//Debug.Log(rt_input + " y");
		}

		lt_input = Input.GetButton("LT");
		lt_axis = Input.GetAxis("LT");
		if (lt_axis != 0){
			lt_input = true;
			//Debug.Log(lt_input + " n");
		}
	}

	void UpdateStates(){
		//states.horizontal = horizontal;
		//states.vertical = vertical;
		states.rt = rt_input;
		states.lt = lt_input;
		states.rb = rb_input;
		states.lb = lb_input;


	}
}
